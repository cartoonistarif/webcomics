# Transcript of Pepper&Carrot Episode 06 [hu]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cím|1|False|6. rész: A bájitalverseny
<hidden>|2|False|Paprika
<hidden>|3|False|Sárgarépa

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|True|A francba, elalvás előtt megint nyitva hagytam az ablakot...
Paprika|2|True|...nagy a huzat...
Paprika|3|False|...és miért látom Komonát az ablakból?
Paprika|4|False|Komona!
Paprika|5|True|A bájital-verseny!
Paprika|6|False|Vé-... véletlenül elaludtam!*
Paprika|9|True|De...
Paprika|10|False|Hol vagyok?
Madár|12|False|háp?|nowhitespace
Jegyzet|7|False|*4. rész: Isteni Szikra

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|False|!!!
Paprika|2|False|Sárgarépa! Nagyon édes vagy, hogy eszedbe jutott elvinni a versenyre!
Paprika|3|False|Fan-tasz-ti-kus!
Paprika|4|True|Itt vannak a ruháim, a sapkám... még egy bájitalt is bekészítettél!
Paprika|5|False|...lássuk, melyiket...

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|False|Micsoda?!!!!
Komona polgármestere|3|False|Komona polgármestereként a bájitalversenyt ezennel megnyitom!
Komona polgármestere|4|False|A városunk el van ragadtatva, hogy nem kevesebb, mint négy boszorkányt láthat vendégül az első versenyen!
Komona polgármestere|5|True|Kérek egy
Komona polgármestere|6|True|hatalmas
Írta|2|False|Komona Bájital Versenye
Komona polgármestere|7|False|tapsot a versenyzőinknek!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Meghallgatás|29|False|Taps
Komona polgármestere|1|True|Messziről, a nagy Technológus Unióból érkezett. Megtisztelő, hogy a versenyen fogadhatjuk az elragadó és leleményes
Komona polgármestere|3|True|Ne feledjük el a helyi lányt, Komona saját boszorkányát,
Komona polgármestere|5|True|A harmadik versenyzőnk a lenyugvó hold földjéről érkezett,
Komona polgármestere|7|True|Végezetül, az utolsó versenyzőnk, Mókuslak erdejéből,
Komona polgármestere|2|False|Koriandert!
Komona polgármestere|4|False|Sáfrányt!
Komona polgármestere|6|False|Shichimi!
Komona polgármestere|8|False|Paprika!
Komona polgármestere|9|True|Kezdődjön a verseny!
Komona polgármestere|10|False|Az eredményt a taps hangereje fogja eldönteni.
Komona polgármestere|11|False|Kezdjük Koriander bemutatójával!
Koriander|13|True|Ne féljenek többé a haláltól, mert itt a...
Koriander|14|False|...Zombifikáló Főzetem!
Meghallgatás|15|True|Taps
Meghallgatás|16|True|Taps
Meghallgatás|17|True|Taps
Meghallgatás|18|True|Taps
Meghallgatás|19|True|Taps
Meghallgatás|20|True|Taps
Meghallgatás|21|True|Taps
Meghallgatás|22|True|Taps
Meghallgatás|23|True|Taps
Meghallgatás|24|True|Taps
Meghallgatás|25|True|Taps
Meghallgatás|26|True|Taps
Meghallgatás|27|True|Taps
Koriander|12|False|Hölgyeim és Uraim!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Komona polgármestere|1|False|Nagyszerű!
Meghallgatás|3|True|Taps
Meghallgatás|4|True|Taps
Meghallgatás|5|True|Taps
Meghallgatás|6|True|Taps
Meghallgatás|7|True|Taps
Meghallgatás|8|True|Taps
Meghallgatás|9|True|Taps
Meghallgatás|10|True|Taps
Meghallgatás|11|True|Taps
Meghallgatás|12|True|Taps
Meghallgatás|13|True|Taps
Meghallgatás|14|True|Taps
Meghallgatás|15|True|Taps
Meghallgatás|16|False|Taps
Sáfrány|18|False|Ugyanis itt az ÉN bájitalom!
Sáfrány|17|True|...de kérem, tartogassák a tapsukat, Komona polgárai!
Sáfrány|20|False|...amivel iriggyé teszik őket!
Sáfrány|22|False|Sikkes Bájitalból!
Sáfrány|21|True|Ez mind lehetséges, mindössze egyetlen cseppre van szükségük az általam készített...
Meghallgatás|23|True|Taps
Meghallgatás|24|True|Taps
Meghallgatás|26|True|Taps
Meghallgatás|27|True|Taps
Meghallgatás|28|True|Taps
Meghallgatás|29|True|Taps
Meghallgatás|30|True|Taps
Meghallgatás|31|True|Taps
Meghallgatás|32|True|Taps
Meghallgatás|33|True|Taps
Meghallgatás|34|True|Taps
Meghallgatás|35|True|Taps
Meghallgatás|36|True|Taps
Meghallgatás|37|True|Taps
Meghallgatás|38|False|Taps
Komona polgármestere|40|False|Ez a bájital gazdaggá teheti egész Komonát!
Komona polgármestere|39|True|Fantasztikus! Hihetetlen!
Meghallgatás|42|True|Taps
Meghallgatás|43|True|Taps
Meghallgatás|44|True|Taps
Meghallgatás|45|True|Taps
Meghallgatás|46|True|Taps
Meghallgatás|47|True|Taps
Meghallgatás|48|True|Taps
Meghallgatás|49|True|Taps
Meghallgatás|50|True|Taps
Meghallgatás|51|True|Taps
Meghallgatás|52|True|Taps
Meghallgatás|53|True|Taps
Meghallgatás|54|True|Taps
Meghallgatás|55|True|Taps
Meghallgatás|56|True|Taps
Meghallgatás|57|True|Taps
Meghallgatás|58|False|Taps
Komona polgármestere|2|False|Koriander magával a halállal szegült szembe a természetfeletti főzetével!
Sáfrány|19|True|A valódi bájital, amire mindannyian vártak, amivel lenyűgözik majd a szomszédaikat...
Komona polgármestere|41|False|A tapsvihar nem tévedhet, Koriander sajnos kiesett.

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Komona polgármestere|1|False|Ezt a bemutatót már nehéz lesz túlszárnyalnia Shichiminek!
Shichimi|4|True|NEM!
Shichimi|5|True|Nem lehet, túl veszélyes!
Shichimi|6|False|SAJNÁLOM!
Komona polgármestere|3|False|Gyerünk, Shichimi, mindenki rád vár!
Komona polgármestere|7|False|Hölgyeim és Uraim, úgy tűnik Shichimi feladta a versenyt...
Sáfrány|8|False|Add csak ide!
Sáfrány|9|False|...és ne tettesd, hogy ennyire szégyenlős vagy, elrontod a műsort.
Sáfrány|10|False|Mindenki tudja, hogy megnyertem a versenyt, bármit is csináljon a főzeted...
Shichimi|11|False|!!!
Hang|12|False|B Z Z Z I IO O|nowhitespace
Shichimi|15|False|Óriás Szörny Főzet!
Shichimi|2|False|Én... nem tudtam, hogy be kell mutatnunk.
Shichimi|13|True|Óvatosan!!!
Shichimi|14|True|Ez egy

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Madár|1|False|KÁR-KÁR-K ááá ááááá r r rr|nowhitespace
Hang|2|False|BAM!
Paprika|3|True|...ha, menő!
Paprika|5|False|Az én bájitalom legalább egy nevetést megér, mert...
Paprika|4|False|Akkor most én jövök?
Komona polgármestere|6|True|Fuss, te bolond!
Komona polgármestere|7|False|A versenynek vége, menekülj!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|2|False|...ahogy mindig, most is eltűnik mindenki, amikor mi következnénk.
Paprika|1|True|Nesze nekem...
Paprika|4|False|Hozzunk helyre mindent, és menjünk haza!
Paprika|7|True|Te
Paprika|8|False|túlméretezett-sikkes-zombi-kanári!
Paprika|10|False|Szeretnél kipróbálni még egy bájitalt?
Paprika|11|False|Nem, mi?
Paprika|6|False|HÉ!
Hang|9|False|K R AK K!|nowhitespace
Paprika|3|False|De legalább tudom, mit fogok kezdeni a „bájitaloddal”, Répa.

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Paprika|1|True|Nézz ide, és olvasd el a feliratot figyelmesen!
Paprika|2|False|Nem fogok habozni, hogy rád öntsem az egészet, ha nem hagyod el Komonát, de rögtön!
Komona polgármestere|3|True|És mivel megmentette városunkat a veszedelemtől,
Komona polgármestere|4|False|az első díjat Paprikának adjuk a... milyen bájitalért is?
Paprika|7|False|hát... valójában... ez igazából nem is egy bájital, csak a macskám vizeletmintája akkorról, amikor legutóbb az állat-orvosnál jártunk...
Paprika|6|True|Haha! Igen...
Paprika|8|False|...akkor mégsem kell bemutatnom?
Narrátor|9|False|6. rész: A bájitalverseny
Narrátor|10|False|FIN
Írta|5|False|50 000 Ko
Készítők|11|False|2015. március - Grafika és történet: David Revoy - Fordítás: Alex Gryson (angol), Halász Gábor "Hali" (magyar)

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Készítők|1|False|A Paprika&Sárgarépa teljesen ingyenes, nyílt forráskódú és az olvasók szponzorálják. Ezt az epizódot az alábbi 245 pártfogónak köszönhetjük:
Készítők|4|False|https://www.patreon.com/davidrevoy
Készítők|3|False|Te is lehetsz a Paprika&Sárgarépa következő részének a pártfogója:
Készítők|7|False|Ez a rész 100% szabad és nyílt forráskódú eszközökkel készült: Krita, Linux Mint
Készítők|6|False|Nyílt forráskódú: a réteges forrásfájlok és a betűtípusok elérhetőek a hivatalos weboldalon
Készítők|5|False|Licenc: Creative Commons Nevezd meg! Módosíthatod, megoszthatod, eladhatod stb.
Készítők|2|False|Алексей ★ Глеб Бузало ★ 獨孤欣 & 獨弧悦 ★ Addison Lewis ★ A Distinguished Robot ★ Adrian Lord ★ Ahmad Ali ★ Aina Reich ★ Alandran ★ Alan Hardman ★ Albert Westra ★ Alcide ★ Alejandro Flores Prieto ★ Alex ★ Alexander Bülow Tomassen ★ Alexander Sopicki ★ Alexandra Jordan ★ Alex Lusco ★ Alex Silver ★ Alex Vandiver ★ Alfredo ★ Ali Poulton (Aunty Pol) ★ Allan Zieser ★ Andreas Rieger ★ Andrej Kwadrin ★ Andrew ★ Andrew Godfrey ★ Andrey Alekseenko ★ Angela K ★ Anna Orlova ★ Antan Karmola ★ Anthony Edlin ★ Antonio Mendoza ★ Ardash Crowfoot ★ Arjun Chennu ★ Arne Brix ★ Aslak Kjølås-Sæverud ★ Axel Bordelon ★ Axel Philipsenburg ★ Barbix ★ Ben Evans ★ Bernd ★ Betsy Luntao ★ Boonsak Watanavisit ★ Boris Fauret ★ Boudewijn Rempt ★ BoxBoy ★ Brett Smith ★ Brian Behnke ★ Brian Smith ★ Bryan Butler ★ Bui Dang Hai Trieu ★ Carlos Levischi ★ Charlotte Lacombe-bar ★ Chris Radcliff ★ Chris Sakkas ★ Christian Gruenwaldner ★ Christophe Carré ★ Christopher Bates ★ Clara Dexter ★ codl ★ Colby Driedger ★ Conway Scott Smith ★ Cuthbert Williams ★ Cyrille Largillier ★ Cyril Paciullo ★ Damien ★ Daniel ★ Daniel Björkman ★ Danny Grimm ★ David Tang ★ DiCola Jamn ★ Dmitry ★ Donald Hayward ★ Duke ★ Eitan Goldshtrom ★ Enrico Billich ★ Epsilon ★ Eric Schulz ★ Faolan Grady ★ Francois Schnell ★ Garret Patterson ★ Ginny Hendricks ★ GreenAngel5 ★ Grigory Petrov ★ Guillaume ★ Guillaume Ballue ★ Gustav Strömbom ★ Guy Davis ★ Happy Mimic ★ Helmar Suschka ★ Henning Döscher ★ Ilyas ★ Irina Rempt ★ Ivan Korotkov ★ James Frazier ★ Janusz ★ Jared Tritsch ★ JDB ★ Jean-Baptiste Hebbrecht ★ Jean-Gabriel LOQUET ★ Jeffrey Schneider ★ Jessey Wright ★ Jim ★ Jim Street ★ Jiska ★ Joachim Schiele ★ JoÃ£o Luiz Machado Junior ★ Joern Konopka ★ joe rutledge ★ Johanne Thomson ★ John ★ John Urquhart Ferguson ★ Jónatan Nilsson ★ Jonathan Leroy ★ Jonathan Ringstad ★ Jon Brake ★ Jorge Bernal ★ Joseph Bowman ★ Julien Duroure ★ Justus Kat ★ Kai-Ting (Danil) Ko ★ Kasper Hansen ★ Kate ★ Kathryn Wuerstl ★ Ken Mingyuan Xia ★ Kingsquee ★ Kroet ★ Levi Kornelsen ★ Liselle ★ Lorentz Grip ★ L S ★ Luc Stepniewski ★ Luke Hochrein ★ MacCoy ★ Magnus Kronnäs ★ Manuel ★ Marc & Rick ★ Marcus ★ Martin Owens ★ Mary Brownlee ★ Masked Admirer ★ Mathias Stærk ★ Mefflin Ross Bullis-bates ★ Michael ★ Michael Gill ★ Michael Pureka ★ Michelle Pereira Garcia ★ Mike Mosher ★ Miroslav ★ Muzyka Dmytro ★ Nataya Castillo ★ Nazhif ★ Nicholas DeLateur ★ Nicholas Terranova ★ Nicki Aya ★ Nicola Angel ★ Nicolae Berbece ★ Nicole Heersema ★ Nielas Sinclair ★ NinjaKnight Comics ★ Noble Hays ★ Noelia Calles Marcos ★ Nora Czaykowski ★ Nyx ★ Olivier Amrein ★ Olivier Brun ★ Omar Willey ★ Oscar Moreno ★ Öykü Su Gürler ★ Ozone S. ★ Pablo Lopez Soriano ★ Pat David ★ Patrick Gamblin ★ Paul ★ Pavel Semenov ★ Pet0r ★ Peter ★ Peter Moonen ★ Petr Vlašic ★ Philippe Jean Edward Bateman ★ Pierre Geier ★ Pierre Vuillemin ★ Pranab Shenoy ★ Pyves & Ran ★ Raghavendra Kamath ★ Rajul Gupta ★ Reorx Meng ★ Ret Samys ★ Rictic ★ RJ van der Weide ★ Roberto Zaghis ★ Roman ★ Rumiko Hoshino ★ Rustin Simons ★ Sally Bridgewater ★ Sami T ★ Samuel Mitson ★ Scott Petrovic ★ Sean Adams ★ Shadefalcon ★ ShadowMist ★ Shafak ★ Shawn Meyer ★ Simon Forster ★ Simon Isenberg ★ Sonny W. ★ Soriac ★ Stanislav Vodetskyi ★ Stephanie Cheshire ★ Stephen Bates ★ Stephen Smoogen ★ Steven Bennett ★ Stuart Dickson ★ Surt ★ TamaskanLEM ★ Tar8156 ★ Terry Hancock ★ TheFaico ★ Thomas Citharel ★ Thomas Courbon ★ Thomas Schwery ★ Tim Burbank ★ Tim J. ★ Tomas Hajek ★ Tom Demian ★ Tom Savage ★ Tracey Reuben ★ Travis Humble ★ Tree ★ Tyson Tan ★ Urm ★ Victoria ★ Victoria White ★ Vladislav Kurdyukov ★ Vlad Tomash ★ Westen Curry ★ Witt N. Vest ★ Xavier Claude ★ Yalyn Vinkindo ★ Yaroslav ★ Zeni Pong ★ Źmicier Kušnaroŭ
