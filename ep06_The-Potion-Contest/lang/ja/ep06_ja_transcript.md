# Transcript of Pepper&Carrot Episode 06 [ja]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|エピソード 6: 魔法薬コンテスト

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
ペッパー|1|True|あれ、しまった また窓を開けたまま 寝ちゃったみたい...
ペッパー|2|True|... 強い風の音 ...
ペッパー|3|False|... それにコモナの街が 外に見える どうして？
ペッパー|4|False|コモナ!
ペッパー|5|False|あの魔法薬 コンテスト!
ペッパー|6|True|きっと... うっかりして 寝ちゃったんだ!
ペッパー|9|True|... でも?
ペッパー|10|False|どこにいるの 私 ?!?
Bird|12|False|ッ?|nowhitespace
Bird|11|True|クワ|nowhitespace
ペッパー|7|False|*
Note|8|False|* エピソード4 : 天才のひらめき を参照

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
ペッパー|1|False|!!!
ペッパー|2|False|キャロット！ コンテストに運べばいい そう思ったのね！ ホントに素敵！
ペッパー|3|False|もう最高 !
ペッパー|4|True|私の魔法薬に、服に、 帽子まで全部 持ってきてくれて...
ペッパー|5|False|... 持ってきた薬を 見てみましょう ...

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
ペッパー|1|False|ええっ ?!!
Mayor of Komona|3|False|今ここに... コモナ市長の私が 魔法薬コンテストの 開会を宣言します!
Mayor of Komona|4|False|記念すべき第1回の開催に 4人もの魔女が 参加してくださったことを 街として歓迎しましょう
Mayor of Komona|5|True|さあ皆さん
Mayor of Komona|6|True|盛大なる拍手で
Writing|2|False|Komona Potion Contest
Mayor of Komona|7|False|お迎えください :

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Audience|29|False|パチパチ
Mayor of Komona|1|True|偉大なる技術者連合から やってきました 魅惑の天才を迎えることができて光栄です
Mayor of Komona|3|True|... 我が街コモナの 魔女を忘れるなど ありえません
Mayor of Komona|5|True|... ３人目の参加者は 沈む月の土地から やってきました！
Mayor of Komona|7|True|... そして、 最後の参加者は リスのはずれの森 からきました
Mayor of Komona|2|False|コリアンダー !
Mayor of Komona|4|False|サフラン !
Mayor of Komona|6|False|シチミ !
Mayor of Komona|8|False|ペッパー !
Mayor of Komona|9|True|コンテスト 開始です!
Mayor of Komona|10|False|拍手メーターで 投票が行われます
Mayor of Komona|11|False|まず最初は コリアンダーによる 実演です
コリアンダー|13|False|... もう死を恐れる 必要はありません ...
コリアンダー|14|True|私の魔法薬があれば！
コリアンダー|15|False|ゾンビ化の薬です !
Audience|16|True|パチパチ
Audience|17|True|パチパチ
Audience|18|True|パチパチ
Audience|19|True|パチパチ
Audience|20|True|パチパチ
Audience|21|True|パチパチ
Audience|22|True|パチパチ
Audience|23|True|パチパチ
Audience|24|True|パチパチ
Audience|25|True|パチパチ
Audience|26|True|パチパチ
Audience|27|True|パチパチ
Audience|28|True|パチパチ
コリアンダー|12|False|ご参加の 紳士淑女の 皆様...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|素晴らしい !
Audience|3|True|パチパチ
Audience|4|True|パチパチ
Audience|5|True|パチパチ
Audience|6|True|パチパチ
Audience|7|True|パチパチ
Audience|8|True|パチパチ
Audience|9|True|パチパチ
Audience|10|True|パチパチ
Audience|11|True|パチパチ
Audience|12|True|パチパチ
Audience|13|True|パチパチ
Audience|14|True|パチパチ
Audience|15|True|パチパチ
Audience|16|False|パチパチ
サフラン|18|True|ここにあるのは
サフラン|17|True|... コモナの街の皆様 拍手をするのを お待ちくださいな !
サフラン|22|False|...皆があなたを うらやむのです!
サフラン|19|True|私の
サフラン|25|False|ゴージャスの魔法薬 !
サフラン|24|True|... ただ一滴垂らすだけで それが可能になる そう、これが私の魔法薬 ...
Audience|26|True|パチパチ
Audience|27|True|パチパチ
Audience|28|True|パチパチ
Audience|29|True|パチパチ
Audience|30|True|パチパチ
Audience|31|True|パチパチ
Audience|32|True|パチパチ
Audience|33|True|パチパチ
Audience|34|True|パチパチ
Audience|35|True|パチパチ
Audience|36|True|パチパチ
Audience|37|True|パチパチ
Audience|38|True|パチパチ
Audience|39|True|パチパチ
Audience|40|False|パチパチ
Mayor of Komona|42|False|この薬はコモナ全体を 裕福に出来るでしょう！
Mayor of Komona|41|True|素晴らしい! びっくりです!
Audience|44|True|パチパチ
Audience|45|True|パチパチ
Audience|46|True|パチパチ
Audience|47|True|パチパチ
Audience|48|True|パチパチ
Audience|49|True|パチパチ
Audience|50|True|パチパチ
Audience|51|True|パチパチ
Audience|52|True|パチパチ
Audience|53|True|パチパチ
Audience|54|True|パチパチ
Audience|55|True|パチパチ
Audience|56|True|パチパチ
Audience|57|True|パチパチ
Audience|58|True|パチパチ
Audience|59|True|パチパチ
Audience|60|False|パチパチ
Mayor of Komona|2|False|コリアンダーは この奇跡の魔法薬で 死を否定しました!
サフラン|21|True|皆様が待っていたのは この魔法薬ですわ 隣人達はあなたの変貌に ビックリ仰天して ...
Mayor of Komona|43|False|皆様の拍手も もっともですね コリアンダーの負けは 既に確定です
サフラン|20|False|魔法薬

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|先ほどの実演の後では シチミが勝つのは 難しそうになってきました!
シチミ|4|True|ダメ!
シチミ|5|True|私...出来ません これは危険なんです
シチミ|6|False|ごめんなさい!
Mayor of Komona|3|False|... さあさあ シチミ、 皆が実演を 待ってますよ
Mayor of Komona|7|False|紳士淑女の皆様 どうやら、シチミは 棄権するようです...
サフラン|8|False|およこし なさい!
サフラン|9|False|... 内気なふりは おやめなさい、 あなたはコンテストを 台無しにしているわ
サフラン|10|False|あなたの魔法薬が どんなものであろうと 私の勝ちだって わかりきってるのよ...
シチミ|11|False|!!!
Sound|12|False|グ アオ オ オオオ|nowhitespace
シチミ|15|False|巨大モンスターの薬なの !
シチミ|2|False|私... 実演しないと いけないなんて 知らなかった...
シチミ|13|True|気をつけて!!!
シチミ|14|True|この魔法薬は

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bird|1|False|ガァ ガァ ガァァァ|nowhitespace
Sound|2|False|バンッ!
ペッパー|3|True|...うん、スゴイ!
ペッパー|5|False|... 私の魔法薬は ちょっとは笑えるはず、 というのも....
ペッパー|4|False|これで私の番かな ?
Mayor of Komona|6|True|逃げるんだ!
Mayor of Komona|7|False|コンテストは おしまいだ ! ... 安全な所へ逃げろ!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
ペッパー|2|False|... いつも通り 私たちの番になると 誰もいなくなっちゃう
ペッパー|1|True|さてと ...
ペッパー|4|True|でも、キャロット お前が持ってきた 『魔法薬』の使い道を 1つは思いついたの
ペッパー|5|False|...この事態を なんとか収拾して 家に帰りましょう！
ペッパー|7|True|そこの
ペッパー|8|False|巨大ゴージャスゾンビカナリアさん!
ペッパー|10|False|最後の魔法薬を 試してみたい? ...
ペッパー|11|False|... いやかしら、 そうね ?
ペッパー|6|False|ねえ !
Sound|9|False|バーン !|nowhitespace

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
ペッパー|1|True|そうね、 ラベルを見て、 しっかりと ...
ペッパー|2|False|... コモナから今すぐ 立ち去らなかったら これをあなたの全身に かけちゃうから !
Mayor of Komona|3|True|街を危機的状況から 救ったので
Mayor of Komona|4|False|優勝はペッパー そしてペッパーの 魔法薬は ... ??!!
ペッパー|7|False|... ええと... 実は、これは... 魔法薬じゃないんです この前獣医に行った時の 私の猫の尿の試料です!
ペッパー|6|True|... アハハ! ううん ...
ペッパー|8|False|... 実演は しますか?...
Narrator|9|False|エピソード 6 : 魔法薬コンテスト
Narrator|10|False|おしまい
Writing|5|False|50,000 Ko
Credits|11|False|March 2015 - Artwork and story by David Revoy - Translation by guruguru

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Pepper&Carrotは完全にフリーでオープンソースで、読者パトロンの支援に 支えられています。このエピソードへの254人のパトロンによる支援に感謝します :
Credits|4|False|https://www.patreon.com/davidrevoy
Credits|3|False|あなたもPepper&Carrotの 次のエピソードのパトロンになれます :
Credits|7|False|ツール : このエピソードは100%フリーでオープンソースなツールで製作されています Linux Mint環境のKrita
Credits|6|False|オープンソース : レイヤーを含むソースファイルすべて、フォントも オフィシャルサイトからダウンロード可能です
Credits|5|False|License : Creative Commons Attribution 変更も、再投稿による共有も、商用利用などなども可能です...
Credits|2|False|Алексей ★ Глеб Бузало ★ 獨孤欣 & 獨弧悦 ★ Addison Lewis ★ A Distinguished Robot ★ Adrian Lord ★ Ahmad Ali ★ Aina Reich ★ Alandran ★ Alan Hardman ★ Albert Westra ★ Alcide ★ Alejandro Flores Prieto ★ Alex ★ Alexander Bülow Tomassen ★ Alexander Sopicki ★ Alexandra Jordan ★ Alex Lusco ★ Alex Silver ★ Alex Vandiver ★ Alfredo ★ Ali Poulton (Aunty Pol) ★ Allan Zieser ★ Andreas Rieger ★ Andrej Kwadrin ★ Andrew ★ Andrew Godfrey ★ Andrey Alekseenko ★ Angela K ★ Anna Orlova ★ Antan Karmola ★ Anthony Edlin ★ Antonio Mendoza ★ Ardash Crowfoot ★ Arjun Chennu ★ Arne Brix ★ Aslak Kjølås-Sæverud ★ Axel Bordelon ★ Axel Philipsenburg ★ Barbix ★ Ben Evans ★ Bernd ★ Betsy Luntao ★ Boonsak Watanavisit ★ Boris Fauret ★ Boudewijn Rempt ★ BoxBoy ★ Brett Smith ★ Brian Behnke ★ Brian Smith ★ Bryan Butler ★ Bui Dang Hai Trieu ★ Carlos Levischi ★ Charlotte Lacombe-bar ★ Chris Radcliff ★ Chris Sakkas ★ Christian Gruenwaldner ★ Christophe Carré ★ Christopher Bates ★ Clara Dexter ★ codl ★ Colby Driedger ★ Conway Scott Smith ★ Cuthbert Williams ★ Cyrille Largillier ★ Cyril Paciullo ★ Damien ★ Daniel ★ Daniel Björkman ★ Danny Grimm ★ David Tang ★ DiCola Jamn ★ Dmitry ★ Donald Hayward ★ Duke ★ Eitan Goldshtrom ★ Enrico Billich ★ Epsilon ★ Eric Schulz ★ Faolan Grady ★ Francois Schnell ★ Garret Patterson ★ Ginny Hendricks ★ GreenAngel5 ★ Grigory Petrov ★ Guillaume ★ Guillaume Ballue ★ Gustav Strömbom ★ Guy Davis ★ Happy Mimic ★ Helmar Suschka ★ Henning Döscher ★ Ilyas ★ Irina Rempt ★ Ivan Korotkov ★ James Frazier ★ Janusz ★ Jared Tritsch ★ JDB ★ Jean-Baptiste Hebbrecht ★ Jean-Gabriel LOQUET ★ Jeffrey Schneider ★ Jessey Wright ★ Jim ★ Jim Street ★ Jiska ★ Joachim Schiele ★ JoÃ£o Luiz Machado Junior ★ Joern Konopka ★ joe rutledge ★ Johanne Thomson ★ John ★ John Urquhart Ferguson ★ Jónatan Nilsson ★ Jonathan Leroy ★ Jonathan Ringstad ★ Jon Brake ★ Jorge Bernal ★ Joseph Bowman ★ Julien Duroure ★ Justus Kat ★ Kai-Ting (Danil) Ko ★ Kasper Hansen ★ Kate ★ Kathryn Wuerstl ★ Ken Mingyuan Xia ★ Kingsquee ★ Kroet ★ Levi Kornelsen ★ Liselle ★ Lorentz Grip ★ L S ★ Luc Stepniewski ★ Luke Hochrein ★ MacCoy ★ Magnus Kronnäs ★ Manuel ★ Marc & Rick ★ Marcus ★ Martin Owens ★ Mary Brownlee ★ Masked Admirer ★ Mathias Stærk ★ Mefflin Ross Bullis-bates ★ Michael ★ Michael Gill ★ Michael Pureka ★ Michelle Pereira Garcia ★ Mike Mosher ★ Miroslav ★ Muzyka Dmytro ★ Nataya Castillo ★ Nazhif ★ Nicholas DeLateur ★ Nicholas Terranova ★ Nicki Aya ★ Nicola Angel ★ Nicolae Berbece ★ Nicole Heersema ★ Nielas Sinclair ★ NinjaKnight Comics ★ Noble Hays ★ Noelia Calles Marcos ★ Nora Czaykowski ★ Nyx ★ Olivier Amrein ★ Olivier Brun ★ Omar Willey ★ Oscar Moreno ★ Öykü Su Gürler ★ Ozone S. ★ Pablo Lopez Soriano ★ Pat David ★ Patrick Gamblin ★ Paul ★ Pavel Semenov ★ Pet0r ★ Peter ★ Peter Moonen ★ Petr Vlašic ★ Philippe Jean Edward Bateman ★ Pierre Geier ★ Pierre Vuillemin ★ Pranab Shenoy ★ Pyves & Ran ★ Raghavendra Kamath ★ Rajul Gupta ★ Reorx Meng ★ Ret Samys ★ Rictic ★ RJ van der Weide ★ Roberto Zaghis ★ Roman ★ Rumiko Hoshino ★ Rustin Simons ★ Sally Bridgewater ★ Sami T ★ Samuel Mitson ★ Scott Petrovic ★ Sean Adams ★ Shadefalcon ★ ShadowMist ★ Shafak ★ Shawn Meyer ★ Simon Forster ★ Simon Isenberg ★ Sonny W. ★ Soriac ★ Stanislav Vodetskyi ★ Stephanie Cheshire ★ Stephen Bates ★ Stephen Smoogen ★ Steven Bennett ★ Stuart Dickson ★ Surt ★ TamaskanLEM ★ Tar8156 ★ Terry Hancock ★ TheFaico ★ Thomas Citharel ★ Thomas Courbon ★ Thomas Schwery ★ Tim Burbank ★ Tim J. ★ Tomas Hajek ★ Tom Demian ★ Tom Savage ★ Tracey Reuben ★ Travis Humble ★ Tree ★ Tyson Tan ★ Urm ★ Victoria ★ Victoria White ★ Vladislav Kurdyukov ★ Vlad Tomash ★ Westen Curry ★ Witt N. Vest ★ Xavier Claude ★ Yalyn Vinkindo ★ Yaroslav ★ Zeni Pong ★ Źmicier Kušnaroŭ
