# Transcript of Pepper&Carrot Episode 06 [ro]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Episodul 6: Concursul Poțiunilor

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Oh vai, am adormit cu fereastra deschisă, din nou...
Pepper|2|True|...ce vânt e...
Pepper|3|False|...și de ce oare pot să văd Komona prin fereastră?
Pepper|4|False|KOMONA!
Pepper|5|False|Concursul Poțiunilor!
Pepper|6|True|Cred că... cred că m-a furat somnul!
Pepper|9|True|...dar?
Pepper|10|False|Unde sunt?!?
Bird|12|False|Mac?|nowhitespace
<hidden>|11|True|*|nowhitespace
<hidden>|7|False|* See Episode 4: Stroke of Genius

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|!!!
Pepper|2|False|Carrot! Ce drăguț din partea ta că te-ai gândit să mă duci la Concurs!
Pepper|3|False|Fan-tas-tic !
Pepper|4|True|Te-ai gândit chiar și să iei o poțiune, hainele mele, și pălăria...
Pepper|5|False|...ia să vedem ce poțiune ai luat...

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|CE?!!
Mayor of Komona|3|False|În calitate de Primar al Komonei, declar concursul poțiunilor... Deschis!
Mayor of Komona|4|False|Orașul nostru este încântat să întâmpine la prima lui ediție nu mai puțin de patru vrăijtoare.
Mayor of Komona|5|True|Vă rog, aplauze
Mayor of Komona|6|True|uriașe
Writing|2|False|Concursul Poțiunilor Komona
Mayor of Komona|7|False|pentru:

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Audience|29|False|Clap
Mayor of Komona|1|True|Din îndepărtata Uniune a Tehnologului, este o onoare să o întâmpin pe superba și ingenioasa
Mayor of Komona|3|True|...fără să uităm de un localnic aparte, vrăjitoarea din Komona
Mayor of Komona|5|True|...Cel de-al treilea participant vine din ținutul lunilor care apun,
Mayor of Komona|7|True|...și la final, ultimul nostru participant, din pădurea Capătul Veveriței
Mayor of Komona|2|False|Coriander!
Mayor of Komona|4|False|Saffron!
Mayor of Komona|6|False|Shichimi!
Mayor of Komona|8|False|Pepper!
Mayor of Komona|9|True|Să înceapă jocurile!
Mayor of Komona|10|False|Votul va fi prin Aplauz-o-metru
Mayor of Komona|11|False|Prima, demonstrația lui Coriander
Coriander|13|False|...frica de moarte este de domeniul trecutului, datorită...
Coriander|14|True|...Poțiunii de
Coriander|15|False|ZOMBIFICARE!
Audience|16|True|Clap
Audience|17|True|Clap
Audience|18|True|Clap
Audience|19|True|Clap
Audience|20|True|Clap
Audience|21|True|Clap
Audience|22|True|Clap
Audience|23|True|Clap
Audience|24|True|Clap
Audience|25|True|Clap
Audience|26|True|Clap
Audience|27|True|Clap
Audience|28|True|Clap
Coriander|12|False|Doamnelor & Domnilor...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|FANTASTIC !
Audience|3|True|Clap
Audience|4|True|Clap
Audience|5|True|Clap
Audience|6|True|Clap
Audience|7|True|Clap
Audience|8|True|Clap
Audience|9|True|Clap
Audience|10|True|Clap
Audience|11|True|Clap
Audience|12|True|Clap
Audience|13|True|Clap
Audience|14|True|Clap
Audience|15|True|Clap
Audience|16|False|Clap
Saffron|18|False|pentru că iată Poțiunea MEA
Saffron|17|True|..dar vă rog, oameni din Komona, nu vă grăbiți cu aplauzele!
Saffron|20|False|...și care îi va face geloși!
Saffron|23|False|EXTRAVAGANȚĂ!
Saffron|22|True|... Poțiunea de
Saffron|21|True|...toate fiind posibile doar aplicând o picătură din...
Audience|25|True|Clap
Audience|26|True|Clap
Audience|27|True|Clap
Audience|28|True|Clap
Audience|29|True|Clap
Audience|30|True|Clap
Audience|31|True|Clap
Audience|32|True|Clap
Audience|33|True|Clap
Audience|34|True|Clap
Audience|35|True|Clap
Audience|36|True|Clap
Audience|37|True|Clap
Audience|38|True|Clap
Audience|39|False|Clap
Mayor of Komona|41|False|Această poțiune ar putea face Komona bogată!
Mayor of Komona|40|True|Fantastic! Incredibil!
Audience|43|True|Clap
Audience|44|True|Clap
Audience|45|True|Clap
Audience|46|True|Clap
Audience|47|True|Clap
Audience|48|True|Clap
Audience|49|True|Clap
Audience|50|True|Clap
Audience|51|True|Clap
Audience|52|True|Clap
Audience|53|True|Clap
Audience|54|True|Clap
Audience|55|True|Clap
Audience|56|True|Clap
Audience|57|True|Clap
Audience|58|True|Clap
Audience|59|False|Clap
Mayor of Komona|2|False|Coriander înfruntă moartea însăși cu această poțiune mi-ra-cu-loa-să!
Saffron|19|True|Adevărata poțiune așteptată de voi: cea care vă va uimi vecinii...
Mayor of Komona|42|False|Cu siguranță nu ați greșit cu aplauzele. Coriander a fost deja eliminată

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|Demonstrația dinainte pare dificil de întrecut pentru Shichimi!
Shichimi|4|True|NU !
Shichimi|5|True|Nu pot, este prea periculos
Shichimi|6|False|SCUZE!
Mayor of Komona|3|False|...haide Shichimi, cu toții stăm după tine
Mayor of Komona|7|False|Se pare, Doamnelor & Domnilor că Shichimi renunță...
Saffron|8|False|Dă-mi aia!
Saffron|9|False|...și nu te mai preface că ești timidă, strici concursul
Saffron|10|False|Toată lumea știe că eu am câștigat, indiferent ce face poțiunea ta...
Shichimi|11|False|!!!
Sound|12|False|B Z Z Z I IO O|nowhitespace
Shichimi|15|False|MONȘTRI GIGANȚI!
Shichimi|2|False|Eu... Eu nu știam că trebuie să și demonstrăm
Shichimi|13|True|AI GRIJĂ!!!
Shichimi|14|True|Este o poțiune de

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bird|1|False|CRA-CRA-Cr a aa aa aaa a a a|nowhitespace
Sound|2|False|BAM!
Pepper|3|True|...hah, tare !
Pepper|5|False|...poțiunea mea o să stârnească vreo câteva hohote, pentru că...
Pepper|4|False|deci acum e rândul meu?
Mayor of Komona|6|True|Fugi, idioato!
Mayor of Komona|7|False|Competiția s-a terminat! ...salvează-te!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|2|False|...ca de obicei, toată lumea pleacă fix când ne vine nouă rândul
Pepper|1|True|și uite așa...
Pepper|4|True|Cel puțin am o idee cum să folosesc "poțiunea" ta, Carrot
Pepper|5|False|...să rezolvăm rapid problemele de aici, după care să mergem acasă!
Pepper|7|True|Tu!
Pepper|8|False|Canar-zombie-fițos-umflat!
Pepper|10|False|Nu vrei să încerci o ultimă poțiune?...
Pepper|11|False|...nu prea, hah?
Pepper|6|False|HEI!
Sound|9|False|C R AC K !|nowhitespace

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Exact, citește cu atenție eticheta...
Pepper|2|False|...O să o torn toată pe tine fără să mă gândesc de două ori dacă nu dispari din Komona imediat!
Mayor of Komona|3|True|Pentru că ne-a salvat orașul de la pericol
Mayor of Komona|4|False|îi acordăm primul loc lui Pepper pentru Poțiunea ei de... ??!!
Pepper|7|False|...hah... de fapt, nu e chiar o poțiune; este o mostră din urina pisicii mele, de la ultima vizită la veterinar!
Pepper|6|True|...Haha! dap...
Pepper|8|False|...așadar, fără demonstrație?...
Narrator|9|False|Episodul 6: Concursul Poțiunilor
Narrator|10|False|SFÂRȘIT
Writing|5|False|50,000 Ko
Credits|11|False|Martie 2015 - Artă și poveste de David Revoy

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Pepper&Carrot este complet gratuit, open-source și sponsorizat cu ajutorul bunăvoinței cititorilor. Pentru acest episod, mulțumiri celor 245 de Patroni:
Credits|4|False|https://www.patreon.com/davidrevoy
Credits|3|False|Și tu poți deveni patron al următorului episod din Pepper&Carrot:
Credits|7|False|Tools: Acest episod a fost realizat 100% cu software Free/Libre Krita on Linux Mint
Credits|6|False|Open-source: toate sursele, cu layer-e și font-uri, sunt disponibile pe site-ul oficial
Credits|5|False|Licență: Creative Commons Attribution Poți modifica, da mai departe, vinde etc. ...
Credits|2|False|Алексей ★ Глеб Бузало ★ 獨孤欣 & 獨弧悦 ★ Addison Lewis ★ A Distinguished Robot ★ Adrian Lord ★ Ahmad Ali ★ Aina Reich ★ Alandran ★ Alan Hardman ★ Albert Westra ★ Alcide ★ Alejandro Flores Prieto ★ Alex ★ Alexander Bülow Tomassen ★ Alexander Sopicki ★ Alexandra Jordan ★ Alex Lusco ★ Alex Silver ★ Alex Vandiver ★ Alfredo ★ Ali Poulton (Aunty Pol) ★ Allan Zieser ★ Andreas Rieger ★ Andrej Kwadrin ★ Andrew ★ Andrew Godfrey ★ Andrey Alekseenko ★ Angela K ★ Anna Orlova ★ Antan Karmola ★ Anthony Edlin ★ Antonio Mendoza ★ Ardash Crowfoot ★ Arjun Chennu ★ Arne Brix ★ Aslak Kjølås-Sæverud ★ Axel Bordelon ★ Axel Philipsenburg ★ Barbix ★ Ben Evans ★ Bernd ★ Betsy Luntao ★ Boonsak Watanavisit ★ Boris Fauret ★ Boudewijn Rempt ★ BoxBoy ★ Brett Smith ★ Brian Behnke ★ Brian Smith ★ Bryan Butler ★ Bui Dang Hai Trieu ★ Carlos Levischi ★ Charlotte Lacombe-bar ★ Chris Radcliff ★ Chris Sakkas ★ Christian Gruenwaldner ★ Christophe Carré ★ Christopher Bates ★ Clara Dexter ★ codl ★ Colby Driedger ★ Conway Scott Smith ★ Cuthbert Williams ★ Cyrille Largillier ★ Cyril Paciullo ★ Damien ★ Daniel ★ Daniel Björkman ★ Danny Grimm ★ David Tang ★ DiCola Jamn ★ Dmitry ★ Donald Hayward ★ Duke ★ Eitan Goldshtrom ★ Enrico Billich ★ Epsilon ★ Eric Schulz ★ Faolan Grady ★ Francois Schnell ★ Garret Patterson ★ Ginny Hendricks ★ GreenAngel5 ★ Grigory Petrov ★ Guillaume ★ Guillaume Ballue ★ Gustav Strömbom ★ Guy Davis ★ Happy Mimic ★ Helmar Suschka ★ Henning Döscher ★ Ilyas ★ Irina Rempt ★ Ivan Korotkov ★ James Frazier ★ Janusz ★ Jared Tritsch ★ JDB ★ Jean-Baptiste Hebbrecht ★ Jean-Gabriel LOQUET ★ Jeffrey Schneider ★ Jessey Wright ★ Jim ★ Jim Street ★ Jiska ★ Joachim Schiele ★ JoÃ£o Luiz Machado Junior ★ Joern Konopka ★ joe rutledge ★ Johanne Thomson ★ John ★ John Urquhart Ferguson ★ Jónatan Nilsson ★ Jonathan Leroy ★ Jonathan Ringstad ★ Jon Brake ★ Jorge Bernal ★ Joseph Bowman ★ Julien Duroure ★ Justus Kat ★ Kai-Ting (Danil) Ko ★ Kasper Hansen ★ Kate ★ Kathryn Wuerstl ★ Ken Mingyuan Xia ★ Kingsquee ★ Kroet ★ Levi Kornelsen ★ Liselle ★ Lorentz Grip ★ L S ★ Luc Stepniewski ★ Luke Hochrein ★ MacCoy ★ Magnus Kronnäs ★ Manuel ★ Marc & Rick ★ Marcus ★ Martin Owens ★ Mary Brownlee ★ Masked Admirer ★ Mathias Stærk ★ Mefflin Ross Bullis-bates ★ Michael ★ Michael Gill ★ Michael Pureka ★ Michelle Pereira Garcia ★ Mike Mosher ★ Miroslav ★ Muzyka Dmytro ★ Nataya Castillo ★ Nazhif ★ Nicholas DeLateur ★ Nicholas Terranova ★ Nicki Aya ★ Nicola Angel ★ Nicolae Berbece ★ Nicole Heersema ★ Nielas Sinclair ★ NinjaKnight Comics ★ Noble Hays ★ Noelia Calles Marcos ★ Nora Czaykowski ★ Nyx ★ Olivier Amrein ★ Olivier Brun ★ Omar Willey ★ Oscar Moreno ★ Öykü Su Gürler ★ Ozone S. ★ Pablo Lopez Soriano ★ Pat David ★ Patrick Gamblin ★ Paul ★ Pavel Semenov ★ Pet0r ★ Peter ★ Peter Moonen ★ Petr Vlašic ★ Philippe Jean Edward Bateman ★ Pierre Geier ★ Pierre Vuillemin ★ Pranab Shenoy ★ Pyves & Ran ★ Raghavendra Kamath ★ Rajul Gupta ★ Reorx Meng ★ Ret Samys ★ Rictic ★ RJ van der Weide ★ Roberto Zaghis ★ Roman ★ Rumiko Hoshino ★ Rustin Simons ★ Sally Bridgewater ★ Sami T ★ Samuel Mitson ★ Scott Petrovic ★ Sean Adams ★ Shadefalcon ★ ShadowMist ★ Shafak ★ Shawn Meyer ★ Simon Forster ★ Simon Isenberg ★ Sonny W. ★ Soriac ★ Stanislav Vodetskyi ★ Stephanie Cheshire ★ Stephen Bates ★ Stephen Smoogen ★ Steven Bennett ★ Stuart Dickson ★ Surt ★ TamaskanLEM ★ Tar8156 ★ Terry Hancock ★ TheFaico ★ Thomas Citharel ★ Thomas Courbon ★ Thomas Schwery ★ Tim Burbank ★ Tim J. ★ Tomas Hajek ★ Tom Demian ★ Tom Savage ★ Tracey Reuben ★ Travis Humble ★ Tree ★ Tyson Tan ★ Urm ★ Victoria ★ Victoria White ★ Vladislav Kurdyukov ★ Vlad Tomash ★ Westen Curry ★ Witt N. Vest ★ Xavier Claude ★ Yalyn Vinkindo ★ Yaroslav ★ Zeni Pong ★ Źmicier Kušnaroŭ
