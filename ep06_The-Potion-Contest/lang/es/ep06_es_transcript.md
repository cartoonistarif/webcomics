# Transcript of Pepper&Carrot Episode 06 [es]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Episodio 6: El concurso de pociones

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|¡Oh mecachis!, otra vez me he quedado dormida sin cerrar la ventana...
Pepper|2|True|...¡cómo corre el viento!
Pepper|3|False|...¿y estoy viendo Komona por la ventana?
Pepper|4|False|¡KOMONA!
Pepper|5|False|¡El concurso de pociones!
Pepper|6|True|Me... ¡me he quedado dormida!
Pepper|9|True|... ¿pero?
Pepper|10|False|¿¡¿Dónde estoy?!?
Bird|12|False|Cuac|nowhitespace
Pepper|7|False|*|nowhitespace
Note|8|False|* ver episodio 4 : Golpe de genialidad

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|!!!
Pepper|2|False|¡Carrot! ¡Eres tan mono! ¡Me llevas volando al concurso!
Pepper|3|False|¡Mag-ní-fi-co!
Pepper|4|True|Incluso has traído una poción, mi ropa y mi sombrero...
Pepper|5|False|... a ver la poción que has elegido ...

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|¡¡¿QUÉEE?!!
Mayor of Komona|3|False|Como alcalde de Komona, declaro abierto el concurso de pociones
Mayor of Komona|4|False|Nuestra villa se complace en recibir en primicia nada menos que a cuatro concursantes
Mayor of Komona|5|True|Recibid con un
Mayor of Komona|6|False|fuerte aplauso a :
Writing|2|False|Concurso de Pociones de Komona

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Audience|30|False|Clap
Mayor of Komona|1|True|Desde el gran país de la Unión de Tecnólogos, es un honor acoger a la encantadora e ingeniosa
Mayor of Komona|3|True|... sin olvidar a nuestra paisana, la bruja más querida de Komona,
Mayor of Komona|5|True|... nuestra tercera participante viene del país de las puestas de luna,
Mayor of Komona|7|True|...y para terminar, nuestra última participante, del bosque de Punta de Ardilla,
Mayor of Komona|2|False|¡Celandria!
Mayor of Komona|4|False|¡Azafrán!
Mayor of Komona|6|False|¡Shichimi!
Mayor of Komona|8|False|¡Pepper!
Mayor of Komona|9|True|¡Que comience el concurso!
Mayor of Komona|10|False|¡El voto se medirá por aplausómetro!
Mayor of Komona|11|False|Y, para comenzar, veamos la demostración de Celandria
Celandria|13|False|Damas y caballeros...
Celandria|14|True|...ya no hace falta tener miedo a la muerte gracias a...
Celandria|15|True|... ¡mi poción de
Celandria|16|False|ZOMBIFICACIÓN!
Audience|17|True|Clap
Audience|18|True|Clap
Audience|19|True|Clap
Audience|20|True|Clap
Audience|21|True|Clap
Audience|22|True|Clap
Audience|23|True|Clap
Audience|24|True|Clap
Audience|25|True|Clap
Audience|26|True|Clap
Audience|27|True|Clap
Audience|28|True|Clap
Audience|29|True|Clap

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|¡FANTÁSTICO!
Mayor of Komona|2|False|¡Celandria desafía incluso la muerte con esta poción mi-la-gro-sa!
Audience|4|True|Clap
Audience|5|True|Clap
Audience|6|True|Clap
Audience|7|True|Clap
Audience|8|True|Clap
Audience|9|True|Clap
Audience|10|True|Clap
Audience|11|True|Clap
Audience|12|True|Clap
Audience|13|True|Clap
Audience|14|True|Clap
Audience|15|True|Clap
Audience|16|True|Clap
Audience|17|False|Clap
Azafrán|19|True|...y observad
Azafrán|18|False|... por favor, ¡parad con vuestros aplausos, pueblo de Komona!
Azafrán|22|True|La auténtica poción que estábais esperando: la que impresionará a todos vuestros vecinos...
Azafrán|23|False|...¡les hará retorcerse de envidia!
Azafrán|20|True|MI
Azafrán|26|False|BURGUESÍA!
Azafrán|25|True|... ¡poción de
Azafrán|24|True|... todo esto es posible a partir de ahora si aplicamos una sola gota de mi ...
Audience|28|True|Clap
Audience|29|True|Clap
Audience|30|True|Clap
Audience|31|True|Clap
Audience|32|True|Clap
Audience|33|True|Clap
Audience|34|True|Clap
Audience|35|True|Clap
Audience|36|True|Clap
Audience|37|True|Clap
Audience|38|True|Clap
Audience|39|True|Clap
Audience|40|True|Clap
Audience|42|False|Clap
Audience|41|True|Clap
Mayor of Komona|44|False|¡Esta poción podría volver rico a todo el pueblo de Komona!
Mayor of Komona|43|True|¡Fantástico! ¡Increíble!
Audience|46|True|Clap
Audience|47|True|Clap
Audience|48|True|Clap
Audience|49|True|Clap
Audience|50|True|Clap
Audience|51|True|Clap
Audience|52|True|Clap
Audience|53|True|Clap
Audience|54|True|Clap
Audience|55|True|Clap
Audience|56|True|Clap
Audience|57|True|Clap
Audience|58|True|Clap
Audience|59|True|Clap
Audience|60|False|Clap
Audience|3|True|Clap
Audience|27|True|Clap
Mayor of Komona|45|False|Vuestros aplausos no dejan duda; Celandria está ya descartada
Azafrán|21|False|Poción

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|Esta última demostración parece poner las cosas muy difíciles a Shichimi...
Shichimi|4|True|¡NO !
Shichimi|5|True|No puedo, es demasiado peligroso
Shichimi|6|False|¡LO SIENTO!
Mayor of Komona|3|False|...¡vamos Shichimi! Todo el mundo te espera...
Mayor of Komona|7|False|Parece, damas y caballeros, que Shichimi se retira...
Azafrán|8|False|¡Dame eso!
Azafrán|9|False|... deja ya de hacerte la tímida y no estropees el espectáculo
Azafrán|10|False|Todo el mundo sabe ya que he ganado el concurso. Da igual lo que haga tu poción...
Shichimi|11|False|!!!
Sound|12|False|B Z Z Z I IO O|nowhitespace
Shichimi|15|False|¡MONSTRUO GIGANTE!
Shichimi|2|False|Yo... yo no sabía que era necesario hacer una demostración...
Shichimi|13|True|¡¡¡CUIDADO!!!
Shichimi|14|True|Es una poción de

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bird|1|False|CROCRO CRO oo o o o o oo oo|nowhitespace
Sound|2|False|BAM!
Pepper|3|True|... ¡eh, mola!
Pepper|5|False|...mi poción debería al menos haceros reír un rato porque....
Pepper|4|False|...¿ahora me toca a mí?
Mayor of Komona|6|True|¡Huye idiota!
Mayor of Komona|7|False|¡El concurso se suspende! ...¡sálvate si puedes!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|2|False|...como siempre, todo el mundo se va cuando nos toca a nosotros
Pepper|1|True|...pues ya ves:
Pepper|4|True|De todas maneras, tengo una idea sobre lo que podemos hacer con tu "poción", Carrot
Pepper|5|False|...¡pongamos un poco de orden aquí y volvamos a casa!
Pepper|7|False|¡Tú, pajarraco-gigante-zombi-burgués!
Pepper|10|True|¿Te gustaría probar la última poción?...
Pepper|11|False|... ¿más bien no, verdad?
Pepper|6|False|¡EH!
Sound|9|False|C R AC K !|nowhitespace

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Sí, lee bien la etiqueta...
Pepper|2|False|...no dudaré en echártela encima si no te largas ahora mismo de Komona
Mayor of Komona|3|True|Por haber salvado nuestra villa de un desastre,
Mayor of Komona|4|False|otorgamos el primer puesto del concurso a Pepper por su poción de ...??!!
Pepper|7|False|...emm ...de hecho, no es realmente una poción; ¡son las muestras de orina de mi gato para su revisión veterinaria!
Pepper|6|True|... ¡Jaja! Sí ...
Pepper|8|False|...¿no hace falta demostración, verdad?
Narrator|9|False|Episodio 6 : El concurso de pociones
Narrator|10|False|FIN
Writing|5|False|50 000 Ko
Credits|11|False|Marzo de 2015 - Dibujo y Guión : David Revoy - Traducción : TheFaico

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Pepper&Carrot es completamente gratuito, de código abierto, y patrocinado gracias al mecenazgo de sus lectores. Para este episodio, gracias a 245 mecenas :
Credits|4|False|https://www.patreon.com/davidrevoy
Credits|3|False|Tu también puedes ser mecenas de Pepper&Carrot para el próximo episodio :
Credits|7|False|Herramientas : Este episodio ha sido creado al 100% con software libre Krita en Linux Mint (GNU/Linux)
Credits|6|False|Open-source : todas las imágenes, fuentes tipográficas, y demás ficheros están disponibles en la página oficial para su descarga.
Credits|5|False|Licencia : Creative Commons Attribution Puedes modificar, compartir, vender, etc...
Credits|2|False|Алексей ★ Глеб Бузало ★ 獨孤欣 & 獨弧悦 ★ Addison Lewis ★ A Distinguished Robot ★ Adrian Lord ★ Ahmad Ali ★ Aina Reich ★ Alandran ★ Alan Hardman ★ Albert Westra ★ Alcide ★ Alejandro Flores Prieto ★ Alex ★ Alexander Bülow Tomassen ★ Alexander Sopicki ★ Alexandra Jordan ★ Alex Lusco ★ Alex Silver ★ Alex Vandiver ★ Alfredo ★ Ali Poulton (Aunty Pol) ★ Allan Zieser ★ Andreas Rieger ★ Andrej Kwadrin ★ Andrew ★ Andrew Godfrey ★ Andrey Alekseenko ★ Angela K ★ Anna Orlova ★ Antan Karmola ★ Anthony Edlin ★ Antonio Mendoza ★ Ardash Crowfoot ★ Arjun Chennu ★ Arne Brix ★ Aslak Kjølås-Sæverud ★ Axel Bordelon ★ Axel Philipsenburg ★ Barbix ★ Ben Evans ★ Bernd ★ Betsy Luntao ★ Boonsak Watanavisit ★ Boris Fauret ★ Boudewijn Rempt ★ BoxBoy ★ Brett Smith ★ Brian Behnke ★ Brian Smith ★ Bryan Butler ★ Bui Dang Hai Trieu ★ Carlos Levischi ★ Charlotte Lacombe-bar ★ Chris Radcliff ★ Chris Sakkas ★ Christian Gruenwaldner ★ Christophe Carré ★ Christopher Bates ★ Clara Dexter ★ codl ★ Colby Driedger ★ Conway Scott Smith ★ Cuthbert Williams ★ Cyrille Largillier ★ Cyril Paciullo ★ Damien ★ Daniel ★ Daniel Björkman ★ Danny Grimm ★ David Tang ★ DiCola Jamn ★ Dmitry ★ Donald Hayward ★ Duke ★ Eitan Goldshtrom ★ Enrico Billich ★ Epsilon ★ Eric Schulz ★ Faolan Grady ★ Francois Schnell ★ Garret Patterson ★ Ginny Hendricks ★ GreenAngel5 ★ Grigory Petrov ★ Guillaume ★ Guillaume Ballue ★ Gustav Strömbom ★ Guy Davis ★ Happy Mimic ★ Helmar Suschka ★ Henning Döscher ★ Ilyas ★ Irina Rempt ★ Ivan Korotkov ★ James Frazier ★ Janusz ★ Jared Tritsch ★ JDB ★ Jean-Baptiste Hebbrecht ★ Jean-Gabriel LOQUET ★ Jeffrey Schneider ★ Jessey Wright ★ Jim ★ Jim Street ★ Jiska ★ Joachim Schiele ★ JoÃ£o Luiz Machado Junior ★ Joern Konopka ★ joe rutledge ★ Johanne Thomson ★ John ★ John Urquhart Ferguson ★ Jónatan Nilsson ★ Jonathan Leroy ★ Jonathan Ringstad ★ Jon Brake ★ Jorge Bernal ★ Joseph Bowman ★ Julien Duroure ★ Justus Kat ★ Kai-Ting (Danil) Ko ★ Kasper Hansen ★ Kate ★ Kathryn Wuerstl ★ Ken Mingyuan Xia ★ Kingsquee ★ Kroet ★ Levi Kornelsen ★ Liselle ★ Lorentz Grip ★ L S ★ Luc Stepniewski ★ Luke Hochrein ★ MacCoy ★ Magnus Kronnäs ★ Manuel ★ Marc & Rick ★ Marcus ★ Martin Owens ★ Mary Brownlee ★ Masked Admirer ★ Mathias Stærk ★ Mefflin Ross Bullis-bates ★ Michael ★ Michael Gill ★ Michael Pureka ★ Michelle Pereira Garcia ★ Mike Mosher ★ Miroslav ★ Muzyka Dmytro ★ Nataya Castillo ★ Nazhif ★ Nicholas DeLateur ★ Nicholas Terranova ★ Nicki Aya ★ Nicola Angel ★ Nicolae Berbece ★ Nicole Heersema ★ Nielas Sinclair ★ NinjaKnight Comics ★ Noble Hays ★ Noelia Calles Marcos ★ Nora Czaykowski ★ Nyx ★ Olivier Amrein ★ Olivier Brun ★ Omar Willey ★ Oscar Moreno ★ Öykü Su Gürler ★ Ozone S. ★ Pablo Lopez Soriano ★ Pat David ★ Patrick Gamblin ★ Paul ★ Pavel Semenov ★ Pet0r ★ Peter ★ Peter Moonen ★ Petr Vlašic ★ Philippe Jean Edward Bateman ★ Pierre Geier ★ Pierre Vuillemin ★ Pranab Shenoy ★ Pyves & Ran ★ Raghavendra Kamath ★ Rajul Gupta ★ Reorx Meng ★ Ret Samys ★ Rictic ★ RJ van der Weide ★ Roberto Zaghis ★ Roman ★ Rumiko Hoshino ★ Rustin Simons ★ Sally Bridgewater ★ Sami T ★ Samuel Mitson ★ Scott Petrovic ★ Sean Adams ★ Shadefalcon ★ ShadowMist ★ Shafak ★ Shawn Meyer ★ Simon Forster ★ Simon Isenberg ★ Sonny W. ★ Soriac ★ Stanislav Vodetskyi ★ Stephanie Cheshire ★ Stephen Bates ★ Stephen Smoogen ★ Steven Bennett ★ Stuart Dickson ★ Surt ★ TamaskanLEM ★ Tar8156 ★ Terry Hancock ★ TheFaico ★ Thomas Citharel ★ Thomas Courbon ★ Thomas Schwery ★ Tim Burbank ★ Tim J. ★ Tomas Hajek ★ Tom Demian ★ Tom Savage ★ Tracey Reuben ★ Travis Humble ★ Tree ★ Tyson Tan ★ Urm ★ Victoria ★ Victoria White ★ Vladislav Kurdyukov ★ Vlad Tomash ★ Westen Curry ★ Witt N. Vest ★ Xavier Claude ★ Yalyn Vinkindo ★ Yaroslav ★ Zeni Pong ★ Źmicier Kušnaroŭ
