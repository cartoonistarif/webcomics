# Transcript of Pepper&Carrot Episode 06 [it]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Episodio 6: La Gara delle Pozioni

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Oh accidenti, mi sono ancora addormentata con la finestra aperta ...
Pepper|2|True|... ma che vento!
Pepper|3|False|... e perché vedo Komona dalla finestra ?
Pepper|4|False|KOMONA!
Pepper|5|False|La Gara delle Pozioni !
Pepper|6|True|Mi... Mi devo essere addormentata !
Pepper|9|True|... ma ?
Pepper|10|False|Dove sono ?!?
Bird|12|False|K?|nowhitespace
Bird|11|True|qua|nowhitespace
Pepper|7|False|*
Note|8|False|* vedi episodio 4 : Colpo di Genio

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|!!!
Pepper|2|False|Carrot! Sei proprio in gamba, mi stai portando alla gara
Pepper|3|False|Ma-gni-fi-co !
Pepper|4|True|Hai pensato anche di portare una pozione, i miei vestiti, e il mio cappello...
Pepper|5|False|.. vediamo che pozione hai preso ...

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|COSA ?!!
Mayor of Komona|3|False|Come sindaco di Komona, dichiaro aperta la Gara delle Pozioni !
Mayor of Komona|4|False|La nostra città è molto orgogliosa di ospitare ben quattro streghe per questa prima edizione
Mayor of Komona|5|True|Facciamo un
Mayor of Komona|6|False|grande applauso :
Writing|2|False|Gara delle Pozioni di Komona

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Audience|30|False|Clap
Mayor of Komona|1|True|Venuta dal grande paese dell'Unione dei Tecnologi, è un onore ospitare l'affascinante e geniale
Mayor of Komona|3|True|... senza dimenticare la nostra concittadina, la vera strega di Komona
Mayor of Komona|5|True|... la nostra terza partecipante viene dal paese della luna calante
Mayor of Komona|7|True|... infine, la nostra ultima partecipante, dalla foresta della Punta di Scoiattolo
Mayor of Komona|2|False|Coriandolo !
Mayor of Komona|4|False|Zafferano !
Mayor of Komona|6|False|Shichimi !
Mayor of Komona|8|False|Pepper !
Mayor of Komona|9|True|Che la gara abbia inizio !
Mayor of Komona|10|False|La votazione avverrà con l'applausometro !
Mayor of Komona|11|False|Per prima, la dimostrazione di Coriandolo
Coriandolo|13|False|Signore e Signori...
Coriandolo|14|True|... non abbiate più paura della morte, grazie alla ...
Coriandolo|15|True|... mia pozione
Coriandolo|16|False|ZOMBIFICANTE !
Audience|17|True|Clap
Audience|18|True|Clap
Audience|19|True|Clap
Audience|20|True|Clap
Audience|21|True|Clap
Audience|22|True|Clap
Audience|23|True|Clap
Audience|24|True|Clap
Audience|25|True|Clap
Audience|26|True|Clap
Audience|27|True|Clap
Audience|28|True|Clap
Audience|29|True|Clap

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|FANTASTICO !
Mayor of Komona|2|False|Coriandolo ha sconfitto la morte stessa con questa pozione mi-ra-co-lo-sa !
Audience|4|True|Clap
Audience|5|True|Clap
Audience|6|True|Clap
Audience|7|True|Clap
Audience|8|True|Clap
Audience|9|True|Clap
Audience|10|True|Clap
Audience|11|True|Clap
Audience|12|True|Clap
Audience|13|True|Clap
Audience|14|True|Clap
Audience|15|True|Clap
Audience|16|True|Clap
Audience|17|False|Clap
Zafferano|19|True|guardate la
Zafferano|18|False|... per favore, risparmiate i vostri applausi, popolo di Komona !
Zafferano|22|True|La vera pozione che tutti state aspettando : quella che farà impressionare tutti i vostri vicini ...
Zafferano|23|False|... li riempirà di invidia !
Zafferano|20|True|MIA
Zafferano|26|False|SCICCHERIA !
Zafferano|25|True|... pozione di
Zafferano|24|True|... tutto questo è possibilie con l'applicazione di una sola goccia della mia ...
Audience|28|True|Clap
Audience|29|True|Clap
Audience|30|True|Clap
Audience|31|True|Clap
Audience|32|True|Clap
Audience|33|True|Clap
Audience|34|True|Clap
Audience|35|True|Clap
Audience|36|True|Clap
Audience|37|True|Clap
Audience|38|True|Clap
Audience|39|True|Clap
Audience|40|True|Clap
Audience|42|False|Clap
Audience|41|True|Clap
Mayor of Komona|44|False|questa pozione renderà ricca tutta Komona !
Mayor of Komona|43|True|Fantastico! Incredibile !
Audience|46|True|Clap
Audience|47|True|Clap
Audience|48|True|Clap
Audience|49|True|Clap
Audience|50|True|Clap
Audience|51|True|Clap
Audience|52|True|Clap
Audience|53|True|Clap
Audience|54|True|Clap
Audience|55|True|Clap
Audience|56|True|Clap
Audience|57|True|Clap
Audience|58|True|Clap
Audience|59|True|Clap
Audience|60|False|Clap
Audience|3|True|Clap
Audience|27|True|Clap
Mayor of Komona|45|False|I vostri applausi non mentono; Coriandolo è già eliminata.
Zafferano|21|False|pozione

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|L'ultima prova sembra difficile da superare per Shichimi !
Shichimi|4|True|NO !
Shichimi|5|True|non posso, è troppo pericoloso
Shichimi|6|False|SCUSATE !
Mayor of Komona|3|False|... allora Shichimi, tutto il mondo sta aspettando
Mayor of Komona|7|False|Mi sembra, Signore e Signori, che Shichimi abbandoni la gara...
Zafferano|8|False|Dammi qua !
Zafferano|9|False|... e smettila di fare la timida e non rovinare lo spettacolo
Zafferano|10|False|Tutto il mondo sa già che ho vinto la gara qualsiasi cosa faccia la tua pozione ...
Shichimi|11|False|!!!
Sound|12|False|B Z Z Z I IO O|nowhitespace
Shichimi|15|False|MOSTRI GIGANTI !
Shichimi|2|False|Io... io non sapevo si dovesse fare una dimonstrazione
Shichimi|13|True|ATTENTIONE!!!
Shichimi|14|True|È una pozione per

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bird|1|False|CRi CRi CRiiii ii ii ii ii ii ii iii i ii|nowhitespace
Sound|2|False|BAM!
Pepper|3|True|... Oh forte !
Pepper|5|False|... la mia pozione si meriterà almeno un paio di risate perché ...
Pepper|4|False|adesso è il mio turno ?
Mayor of Komona|6|True|Scappa idiota!
Mayor of Komona|7|False|il concorso è sospeso ! ... si salvi chi può!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|2|False|...come al solito tutti se ne vanno proprio quando tocca a noi
Pepper|1|True|ecco qua :
Pepper|4|True|Almeno avrei una piccola idea su cosa fare con la tua "pozione", Carrot
Pepper|5|False|...rimettiamo tutto a posto e torniamo a casa!
Pepper|7|True|Tu
Pepper|8|False|Sciccoso-zombie-canarino-gigante !
Pepper|10|False|Ti piacerebbe provare l'ultima pozione ?...
Pepper|11|False|... veramente no grazie ?
Pepper|6|False|HEI !
Sound|9|False|C R AC K !|nowhitespace

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Siii, leggi bene l'étichetta...
Pepper|2|False|.. non ci penserò su due volte di versartela tutta addosso se non te ne vai subito via da Komona adesso !
Mayor of Komona|3|True|Per aver salvato la nostra città da un disastro,
Mayor of Komona|4|False|consegnamo il primo premio a Pepper per la sua Pozione di .. ??!!
Pepper|7|False|... hu... veramente, non è proprio una pozione ; è il campione di urina del mio gatto per il veterinario !
Pepper|6|True|... Haha! si ...
Pepper|8|False|... non volete una dimostrazione vero ?...
Narrator|9|False|Episodio 6 : La Gara delle Pozioni
Narrator|10|False|FINE
Writing|5|False|50 000 Ko
Credits|11|False|Marzo 2015 - Disegni e Sceneggiatura : David Revoy , Traduzione : Carlo Gandolfi

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Pepper&Carrot è completamente gratuito, open-source e sostenuto grazie alle gentili donazioni dei lettori. Per questo episondio un grazie ai 254 sostenitori :
Credits|4|False|https://www.patreon.com/davidrevoy
Credits|3|False|Sostieni il progetto, fai una donazione a Pepper&Carrot per il prossimo episodio :
Credits|7|False|Strumenti : Questo episodio è stato creato con strumenti 100% liberi e open-source Krita su Linux Mint
Credits|6|False|Open-source : tutti i file sorgenti ad alta risoluzione con i fonts disponibili per il download, sul sito ufficiale.
Credits|5|False|Licenza : Creative Commons Attribuzione potete modificare, distribuire, vendere, etc...
Credits|2|False|Алексей ★ Глеб Бузало ★ 獨孤欣 & 獨弧悦 ★ Addison Lewis ★ A Distinguished Robot ★ Adrian Lord ★ Ahmad Ali ★ Aina Reich ★ Alandran ★ Alan Hardman ★ Albert Westra ★ Alcide ★ Alejandro Flores Prieto ★ Alex ★ Alexander Bülow Tomassen ★ Alexander Sopicki ★ Alexandra Jordan ★ Alex Lusco ★ Alex Silver ★ Alex Vandiver ★ Alfredo ★ Ali Poulton (Aunty Pol) ★ Allan Zieser ★ Andreas Rieger ★ Andrej Kwadrin ★ Andrew ★ Andrew Godfrey ★ Andrey Alekseenko ★ Angela K ★ Anna Orlova ★ Antan Karmola ★ Anthony Edlin ★ Antonio Mendoza ★ Ardash Crowfoot ★ Arjun Chennu ★ Arne Brix ★ Aslak Kjølås-Sæverud ★ Axel Bordelon ★ Axel Philipsenburg ★ Barbix ★ Ben Evans ★ Bernd ★ Betsy Luntao ★ Boonsak Watanavisit ★ Boris Fauret ★ Boudewijn Rempt ★ BoxBoy ★ Brett Smith ★ Brian Behnke ★ Brian Smith ★ Bryan Butler ★ Bui Dang Hai Trieu ★ Carlos Levischi ★ Charlotte Lacombe-bar ★ Chris Radcliff ★ Chris Sakkas ★ Christian Gruenwaldner ★ Christophe Carré ★ Christopher Bates ★ Clara Dexter ★ codl ★ Colby Driedger ★ Conway Scott Smith ★ Cuthbert Williams ★ Cyrille Largillier ★ Cyril Paciullo ★ Damien ★ Daniel ★ Daniel Björkman ★ Danny Grimm ★ David Tang ★ DiCola Jamn ★ Dmitry ★ Donald Hayward ★ Duke ★ Eitan Goldshtrom ★ Enrico Billich ★ Epsilon ★ Eric Schulz ★ Faolan Grady ★ Francois Schnell ★ Garret Patterson ★ Ginny Hendricks ★ GreenAngel5 ★ Grigory Petrov ★ Guillaume ★ Guillaume Ballue ★ Gustav Strömbom ★ Guy Davis ★ Happy Mimic ★ Helmar Suschka ★ Henning Döscher ★ Ilyas ★ Irina Rempt ★ Ivan Korotkov ★ James Frazier ★ Janusz ★ Jared Tritsch ★ JDB ★ Jean-Baptiste Hebbrecht ★ Jean-Gabriel LOQUET ★ Jeffrey Schneider ★ Jessey Wright ★ Jim ★ Jim Street ★ Jiska ★ Joachim Schiele ★ JoÃ£o Luiz Machado Junior ★ Joern Konopka ★ joe rutledge ★ Johanne Thomson ★ John ★ John Urquhart Ferguson ★ Jónatan Nilsson ★ Jonathan Leroy ★ Jonathan Ringstad ★ Jon Brake ★ Jorge Bernal ★ Joseph Bowman ★ Julien Duroure ★ Justus Kat ★ Kai-Ting (Danil) Ko ★ Kasper Hansen ★ Kate ★ Kathryn Wuerstl ★ Ken Mingyuan Xia ★ Kingsquee ★ Kroet ★ Levi Kornelsen ★ Liselle ★ Lorentz Grip ★ L S ★ Luc Stepniewski ★ Luke Hochrein ★ MacCoy ★ Magnus Kronnäs ★ Manuel ★ Marc & Rick ★ Marcus ★ Martin Owens ★ Mary Brownlee ★ Masked Admirer ★ Mathias Stærk ★ Mefflin Ross Bullis-bates ★ Michael ★ Michael Gill ★ Michael Pureka ★ Michelle Pereira Garcia ★ Mike Mosher ★ Miroslav ★ Muzyka Dmytro ★ Nataya Castillo ★ Nazhif ★ Nicholas DeLateur ★ Nicholas Terranova ★ Nicki Aya ★ Nicola Angel ★ Nicolae Berbece ★ Nicole Heersema ★ Nielas Sinclair ★ NinjaKnight Comics ★ Noble Hays ★ Noelia Calles Marcos ★ Nora Czaykowski ★ Nyx ★ Olivier Amrein ★ Olivier Brun ★ Omar Willey ★ Oscar Moreno ★ Öykü Su Gürler ★ Ozone S. ★ Pablo Lopez Soriano ★ Pat David ★ Patrick Gamblin ★ Paul ★ Pavel Semenov ★ Pet0r ★ Peter ★ Peter Moonen ★ Petr Vlašic ★ Philippe Jean Edward Bateman ★ Pierre Geier ★ Pierre Vuillemin ★ Pranab Shenoy ★ Pyves & Ran ★ Raghavendra Kamath ★ Rajul Gupta ★ Reorx Meng ★ Ret Samys ★ Rictic ★ RJ van der Weide ★ Roberto Zaghis ★ Roman ★ Rumiko Hoshino ★ Rustin Simons ★ Sally Bridgewater ★ Sami T ★ Samuel Mitson ★ Scott Petrovic ★ Sean Adams ★ Shadefalcon ★ ShadowMist ★ Shafak ★ Shawn Meyer ★ Simon Forster ★ Simon Isenberg ★ Sonny W. ★ Soriac ★ Stanislav Vodetskyi ★ Stephanie Cheshire ★ Stephen Bates ★ Stephen Smoogen ★ Steven Bennett ★ Stuart Dickson ★ Surt ★ TamaskanLEM ★ Tar8156 ★ Terry Hancock ★ TheFaico ★ Thomas Citharel ★ Thomas Courbon ★ Thomas Schwery ★ Tim Burbank ★ Tim J. ★ Tomas Hajek ★ Tom Demian ★ Tom Savage ★ Tracey Reuben ★ Travis Humble ★ Tree ★ Tyson Tan ★ Urm ★ Victoria ★ Victoria White ★ Vladislav Kurdyukov ★ Vlad Tomash ★ Westen Curry ★ Witt N. Vest ★ Xavier Claude ★ Yalyn Vinkindo ★ Yaroslav ★ Zeni Pong ★ Źmicier Kušnaroŭ
