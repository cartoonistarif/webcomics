# Transcript of Pepper&Carrot Episode 06 [pt]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Título|1|False|Episódio 6: O Torneio de Poções

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Puxa vida, caí no sono com a janela aberta outra vez...
Pepper|2|True|...e tá ventando tanto...
Pepper|3|False|...e por que eu estou vendo Komona pela janela?
Pepper|4|False|KOMONA!
Pepper|5|False|O torneio de poções!
Pepper|6|True|Eu devo... devo ter cochilado sem querer!
Pepper|9|True|...mas?
Pepper|10|False|Onde eu estou?!?
Pássaro|12|False|cK?|nowhitespace
Pássaro|11|True|qua|nowhitespace
Pepper|7|False|*
Nota|8|False|* Ver episódio 4 : Ideia de Gênio

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|!!!
Pepper|2|False|Carrot! Você é um fofo por ter me trazido até o torneio!
Pepper|3|False|Fan-tás-ti-co!
Pepper|4|True|Você inclusive trouxe uma poção, minhas roupas e meu chapéu...
Pepper|5|False|...vamos ver que poção você trouxe...

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|O QUÊ?!!
Prefeito de Komona|3|False|Como prefeito de Komona, tenho o prazer de dar início às competições do torneio de poções!
Prefeito de Komona|4|False|É um grande prazer para nossa cidade receber não menos que quatro bruxas para a primeira edição deste torneio.
Prefeito de Komona|5|True|Por favor, uma
Prefeito de Komona|6|True|enorme
Escrita|2|False|Torneio de Poções de Komona
Prefeito de Komona|7|False|salva de palmas...

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Audiência|29|False|Clap
Prefeito de Komona|1|True|Da longínqua União Tecnologica, é uma honra dar as boas-vindas à encantadora e engenhosa,
Prefeito de Komona|3|True|...não vamos esquecer nossa garota, a própria bruxa de Komona,
Prefeito de Komona|5|True|...nossa terceira participante vem nos visitar das terras das luas poentes,
Prefeito de Komona|7|True|...e finalmente, nossa última participante, da floresta do Canto do Esquilo,
Prefeito de Komona|2|False|Coriander!
Prefeito de Komona|4|False|Saffron!
Prefeito de Komona|6|False|Shichimi!
Prefeito de Komona|8|False|Pepper!
Prefeito de Komona|9|True|Que os jogos comecem!
Prefeito de Komona|10|False|A votação se dará pelo aplausômetro
Prefeito de Komona|11|False|Primeiramente, a demonstração da Coriander
Coriander|13|False|...não temam mais a morte, graças a minha...
Coriander|14|True|...Poção
Coriander|15|False|ZUMBIFICADORA!
Audiência|16|True|Clap
Audiência|17|True|Clap
Audiência|18|True|Clap
Audiência|19|True|Clap
Audiência|20|True|Clap
Audiência|21|True|Clap
Audiência|22|True|Clap
Audiência|23|True|Clap
Audiência|24|True|Clap
Audiência|25|True|Clap
Audiência|26|True|Clap
Audiência|27|True|Clap
Audiência|28|True|Clap
Coriander|12|False|Senhoras e Senhores...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Prefeito de Komona|1|False|FANTÁSTICO !
Audiência|3|True|Clap
Audiência|4|True|Clap
Audiência|5|True|Clap
Audiência|6|True|Clap
Audiência|7|True|Clap
Audiência|8|True|Clap
Audiência|9|True|Clap
Audiência|10|True|Clap
Audiência|11|True|Clap
Audiência|12|True|Clap
Audiência|13|True|Clap
Audiência|14|True|Clap
Audiência|15|True|Clap
Audiência|16|False|Clap
Saffron|18|True|...Pois lá vem a
Saffron|17|True|... mas por favor, guardem seus aplausos, povo de Komona!
Saffron|22|False|...deixá-los com inveja!
Saffron|19|True|MINHA POÇÃO!
Saffron|25|False|CHIQUEZA!
Saffron|24|True|... Poção da
Saffron|23|False|...tudo isso é possível com a simples aplicação de uma única gota da minha...
Audiência|26|True|Clap
Audiência|27|True|Clap
Audiência|28|True|Clap
Audiência|29|True|Clap
Audiência|30|True|Clap
Audiência|31|True|Clap
Audiência|32|True|Clap
Audiência|33|True|Clap
Audiência|34|True|Clap
Audiência|35|True|Clap
Audiência|36|True|Clap
Audiência|37|True|Clap
Audiência|38|True|Clap
Audiência|39|True|Clap
Audiência|40|False|Clap
Prefeito de Komona|42|False|Essa poção poderia tornar todos em Komona ricos!
Prefeito de Komona|41|True|Fantástico! Incrível!
Audiência|44|True|Clap
Audiência|45|True|Clap
Audiência|46|True|Clap
Audiência|47|True|Clap
Audiência|48|True|Clap
Audiência|49|True|Clap
Audiência|50|True|Clap
Audiência|51|True|Clap
Audiência|52|True|Clap
Audiência|53|True|Clap
Audiência|54|True|Clap
Audiência|55|True|Clap
Audiência|56|True|Clap
Audiência|57|True|Clap
Audiência|58|True|Clap
Audiência|59|True|Clap
Audiência|60|False|Clap
Prefeito de Komona|2|False|Coriander desafia a própria morte com essa poção mi-la-gro-sa!
Saffron|21|True|A poção pela qual todos vocês estavam esperando: aquela que vai maravilhar seus vizinhos...
Prefeito de Komona|43|False|Seus aplausos não podem estar errados. Coriander já foi eliminada

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Prefeito de Komona|1|False|Vai ser difícil para a Shichimi ganhar depois dessa última apresentação!
Shichimi|4|True|NÃO !
Shichimi|5|True|Eu não posso, é muito perigoso
Shichimi|6|False|DESCULPE !
Prefeito de Komona|3|False|...vamos lá Shichimi, tá todo mundo esperando você
Prefeito de Komona|7|False|Então, Senhoras & Senhores parece que Shichimi desiste...
Saffron|8|False|Me dá isso!
Saffron|9|False|...e pare de se fingir de tímida, você está estragando o show
Saffron|10|False|Todo mundo já sabe que eu ganhei o torneio, então não importa o que sua poção faz...
Shichimi|11|False|!!!
Som|12|False|B Z Z Z I IO O|nowhitespace
Shichimi|15|False|MONSTROS GIGANTES!
Shichimi|2|False|Eu... Eu não sabia que precisaríamos demonstrar nossas poções
Shichimi|13|True|CUIDADO!!!
Shichimi|14|True|É uma poção que cria

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pássaro|1|False|CAW-CAW-C aa aa aa aa w w ww|nowhitespace
Som|2|False|BAM!
Pepper|3|True|...ah, legal!
Pepper|5|False|...pelo menos minha poção vai causar algumas risadas....
Pepper|4|False|Então, é minha vez agora?
Prefeito de Komona|6|True|Corre sua doida!
Prefeito de Komona|7|False|A competição acabou! ...Salve-se!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|2|False|...como sempre, todo mundo vai embora bem na nossa vez
Pepper|1|True|Ai ai...
Pepper|4|True|Pelo menos eu tenho uma ideia do que fazer com a sua "poção", Carrot...
Pepper|5|False|...arrumar as coisas por aqui e voltar pra casa!
Pepper|7|True|Seu
Pepper|8|False|Canário-zumbi-chique-desproporcional!
Pepper|10|False|Quer experimentar uma última poção?...
Pepper|11|False|...não tá muito afim, é?
Pepper|6|False|EI!
Som|9|False|C R AC K !|nowhitespace

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|É... Dá uma olhada no rótulo, com cuidado...
Pepper|2|False|...Eu não vou pensar duas vezes antes de derramar isso em você se você não for embora de Komona agora!
Prefeito de Komona|3|True|Visto que ela salvou nossa cidade quando estávamos em perigo,
Prefeito de Komona|4|False|nós entregamos o primeiro prêmio a Pepper por sua Poção de ...??!!
Pepper|7|False|...hum... ...na verdade, não é bem uma poção, é uma amostra de urina do meu gato, de sua última visita ao veterinário!
Pepper|6|True|...Haha! pois é...
Pepper|8|False|...sem demonstração então?...
Narrador|9|False|Episódio 6: O Torneio de Poções
Narrador|10|False|FIM
Escrita|5|False|50,000 Ko
Créditos|11|False|Março 2015 - Arte por roteiro por David Revoy - Tradução por Frederico Batista

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Créditos|1|False|Pepper&Carrot é inteiramente livre, open-source e patrocinado graças a generosa contribuição dos leitores. Para esse episódio, obrigado aos 245 Patronos :
Créditos|4|False|https://www.patreon.com/davidrevoy
Créditos|3|False|Você também pode ser tornar um patrono de Pepper&Carrot para o próximo episódio:
Créditos|7|False|Ferramentas : Esse episódio foi 100% desenhado com o software Grátis/Livre Krita no Linux Mint
Créditos|6|False|Open-source : todos os arquivos fonte com layers e fontes, estão disponíveis no site oficial
Créditos|5|False|Licença : Creative Commons Attribution Você pode modificar, distribuir, vender etc. ...
Créditos|2|False|Алексей ★ Глеб Бузало ★ 獨孤欣 & 獨弧悦 ★ Addison Lewis ★ A Distinguished Robot ★ Adrian Lord ★ Ahmad Ali ★ Aina Reich ★ Alandran ★ Alan Hardman ★ Albert Westra ★ Alcide ★ Alejandro Flores Prieto ★ Alex ★ Alexander Bülow Tomassen ★ Alexander Sopicki ★ Alexandra Jordan ★ Alex Lusco ★ Alex Silver ★ Alex Vandiver ★ Alfredo ★ Ali Poulton (Aunty Pol) ★ Allan Zieser ★ Andreas Rieger ★ Andrej Kwadrin ★ Andrew ★ Andrew Godfrey ★ Andrey Alekseenko ★ Angela K ★ Anna Orlova ★ Antan Karmola ★ Anthony Edlin ★ Antonio Mendoza ★ Ardash Crowfoot ★ Arjun Chennu ★ Arne Brix ★ Aslak Kjølås-Sæverud ★ Axel Bordelon ★ Axel Philipsenburg ★ Barbix ★ Ben Evans ★ Bernd ★ Betsy Luntao ★ Boonsak Watanavisit ★ Boris Fauret ★ Boudewijn Rempt ★ BoxBoy ★ Brett Smith ★ Brian Behnke ★ Brian Smith ★ Bryan Butler ★ Bui Dang Hai Trieu ★ Carlos Levischi ★ Charlotte Lacombe-bar ★ Chris Radcliff ★ Chris Sakkas ★ Christian Gruenwaldner ★ Christophe Carré ★ Christopher Bates ★ Clara Dexter ★ codl ★ Colby Driedger ★ Conway Scott Smith ★ Cuthbert Williams ★ Cyrille Largillier ★ Cyril Paciullo ★ Damien ★ Daniel ★ Daniel Björkman ★ Danny Grimm ★ David Tang ★ DiCola Jamn ★ Dmitry ★ Donald Hayward ★ Duke ★ Eitan Goldshtrom ★ Enrico Billich ★ Epsilon ★ Eric Schulz ★ Faolan Grady ★ Francois Schnell ★ Garret Patterson ★ Ginny Hendricks ★ GreenAngel5 ★ Grigory Petrov ★ Guillaume ★ Guillaume Ballue ★ Gustav Strömbom ★ Guy Davis ★ Happy Mimic ★ Helmar Suschka ★ Henning Döscher ★ Ilyas ★ Irina Rempt ★ Ivan Korotkov ★ James Frazier ★ Janusz ★ Jared Tritsch ★ JDB ★ Jean-Baptiste Hebbrecht ★ Jean-Gabriel LOQUET ★ Jeffrey Schneider ★ Jessey Wright ★ Jim ★ Jim Street ★ Jiska ★ Joachim Schiele ★ JoÃ£o Luiz Machado Junior ★ Joern Konopka ★ joe rutledge ★ Johanne Thomson ★ John ★ John Urquhart Ferguson ★ Jónatan Nilsson ★ Jonathan Leroy ★ Jonathan Ringstad ★ Jon Brake ★ Jorge Bernal ★ Joseph Bowman ★ Julien Duroure ★ Justus Kat ★ Kai-Ting (Danil) Ko ★ Kasper Hansen ★ Kate ★ Kathryn Wuerstl ★ Ken Mingyuan Xia ★ Kingsquee ★ Kroet ★ Levi Kornelsen ★ Liselle ★ Lorentz Grip ★ L S ★ Luc Stepniewski ★ Luke Hochrein ★ MacCoy ★ Magnus Kronnäs ★ Manuel ★ Marc & Rick ★ Marcus ★ Martin Owens ★ Mary Brownlee ★ Masked Admirer ★ Mathias Stærk ★ Mefflin Ross Bullis-bates ★ Michael ★ Michael Gill ★ Michael Pureka ★ Michelle Pereira Garcia ★ Mike Mosher ★ Miroslav ★ Muzyka Dmytro ★ Nataya Castillo ★ Nazhif ★ Nicholas DeLateur ★ Nicholas Terranova ★ Nicki Aya ★ Nicola Angel ★ Nicolae Berbece ★ Nicole Heersema ★ Nielas Sinclair ★ NinjaKnight Comics ★ Noble Hays ★ Noelia Calles Marcos ★ Nora Czaykowski ★ Nyx ★ Olivier Amrein ★ Olivier Brun ★ Omar Willey ★ Oscar Moreno ★ Öykü Su Gürler ★ Ozone S. ★ Pablo Lopez Soriano ★ Pat David ★ Patrick Gamblin ★ Paul ★ Pavel Semenov ★ Pet0r ★ Peter ★ Peter Moonen ★ Petr Vlašic ★ Philippe Jean Edward Bateman ★ Pierre Geier ★ Pierre Vuillemin ★ Pranab Shenoy ★ Pyves & Ran ★ Raghavendra Kamath ★ Rajul Gupta ★ Reorx Meng ★ Ret Samys ★ Rictic ★ RJ van der Weide ★ Roberto Zaghis ★ Roman ★ Rumiko Hoshino ★ Rustin Simons ★ Sally Bridgewater ★ Sami T ★ Samuel Mitson ★ Scott Petrovic ★ Sean Adams ★ Shadefalcon ★ ShadowMist ★ Shafak ★ Shawn Meyer ★ Simon Forster ★ Simon Isenberg ★ Sonny W. ★ Soriac ★ Stanislav Vodetskyi ★ Stephanie Cheshire ★ Stephen Bates ★ Stephen Smoogen ★ Steven Bennett ★ Stuart Dickson ★ Surt ★ TamaskanLEM ★ Tar8156 ★ Terry Hancock ★ TheFaico ★ Thomas Citharel ★ Thomas Courbon ★ Thomas Schwery ★ Tim Burbank ★ Tim J. ★ Tomas Hajek ★ Tom Demian ★ Tom Savage ★ Tracey Reuben ★ Travis Humble ★ Tree ★ Tyson Tan ★ Urm ★ Victoria ★ Victoria White ★ Vladislav Kurdyukov ★ Vlad Tomash ★ Westen Curry ★ Witt N. Vest ★ Xavier Claude ★ Yalyn Vinkindo ★ Yaroslav ★ Zeni Pong ★ Źmicier Kušnaroŭ
