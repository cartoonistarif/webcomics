# Transcript of Pepper&Carrot Episode 30 [fr]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titre|1|False|Épisode 30 : Besoin d'un Câlin

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrateur|1|False|- FIN -

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|5|True|Vous pouvez aussi devenir mécène de Pepper&Carrot et avoir votre nom inscrit ici !
Pepper|3|True|Pepper&Carrot est entièrement libre, gratuit, open-source et sponsorisé grâce au mécénat de ses lecteurs.
Pepper|4|False|Cet épisode a reçu le soutien de 973 mécènes !
Pepper|7|True|Allez sur www.peppercarrot.com pour plus d'informations !
Pepper|6|True|Nous sommes sur Patreon, Tipeee, PayPal, Liberapay …et d'autres !
Pepper|8|False|Merci !
Pepper|2|True|Le saviez-vous ?
Crédits|1|False|Le 3 Septembre 2019 Dessin, couleurs et scénario : David Revoy. Relecteurs du storyboard : Alina the Hedgehog, Craig Maloney, Jihoon Kim, Parnikkapore, Martin Disch, Nicolas Artance, Valvin. Version française originale Basé sur l'univers d'Hereva Créateur : David Revoy. Mainteneur principal : Craig Maloney. Rédacteurs : Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Correcteurs : Willem Sonke, Moini, Hali, CGand, Alex Gryson . Logiciels : Krita/4.2~git branch, Inkscape 0.92.3 on Kubuntu 18.04.2. Licence : Creative Commons Attribution 4.0. www.peppercarrot.com
<hidden>|0|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|0|False|You can also translate this page if you want.
<hidden>|0|False|Beta readers help with the story, proofreaders give feedback about the text.
