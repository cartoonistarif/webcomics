# Transcript of Pepper&Carrot Episode 02 [uk]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Епізод 2: Веселкові зілля

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|5|True|Бульк
Sound|6|True|Бульк
Sound|7|False|Бульк
Writing|1|True|WARNING
Writing|3|False|PROPERTY
Writing|2|True|WITCH
Writing|4|False|33

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|7|False|цок|nowhitespace
Writing|1|False|FIRE D
Writing|2|False|DEEP OCEAN
Writing|3|False|VIOLE(N)T
Writing|4|False|ULTRA BLUE
Writing|5|False|PIN
Writing|6|False|MSON
Writing|8|False|NATURE
Writing|9|False|YELLOW
Writing|10|False|ORANGE TOP
Writing|11|False|FIRE DANCE
Writing|12|False|DEEP OCEAN
Writing|13|False|VIOLE(N)T
Writing|14|False|ULTRA BLUE
Writing|15|False|META PINK
Writing|16|False|MAGENTA X
Writing|17|False|CRIMSON

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|Бульк
Sound|2|True|Бульк
Sound|3|False|Бульк

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|Бульк
Sound|11|True|К
Sound|12|True|Х|nowhitespace
Sound|13|True|Х|nowhitespace
Sound|14|True|х|nowhitespace
Sound|15|True|х|nowhitespace
Sound|16|True|л|nowhitespace
Sound|17|False|п|nowhitespace
Sound|2|False|Бульк
Sound|3|True|Бульк
Sound|4|False|Бульк
Sound|5|True|Бульк
Sound|6|False|Бульк
Sound|18|False|Кхмм... !
Sound|7|True|П
Sound|8|True|ф|nowhitespace
Sound|9|True|ф|nowhitespace
Sound|10|False|фф !|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|5|True|п|nowhitespace
Sound|4|True|ля|nowhitespace
Sound|3|True|х
Sound|1|True|Буль
Sound|2|False|Буль
Sound|8|True|п|nowhitespace
Sound|7|True|ля|nowhitespace
Sound|6|True|х
Sound|11|False|п|nowhitespace
Sound|10|True|ля|nowhitespace
Sound|9|True|х

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|True|This webcomic is open-source and this episode was funded by my 21 patrons on
Credits|2|False|www.patreon.com/davidrevoy
Credits|3|False|Many thanks to
Credits|4|False|made with Krita on GNU/Linux
