# Transcript of Pepper&Carrot Episode 02 [tp]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
nimi|1|False|wan nanpa tu: telo pi kule mute

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
kalama|6|True|Kulu
kalama|7|True|Kulu
kalama|8|False|Kulu
sitelen|1|True|O LUKIN!
sitelen|4|False|LI LON
sitelen|2|True|JAN PI
sitelen|5|False|33
sitelen|3|True|WAWA NASA

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
kalama|7|False|tok|nowhitespace
sitelen|1|False|SELI T
sitelen|2|False|TELO ANPA
sitelen|3|False|UTALA(SO)
sitelen|4|False|LASO WAWA
sitelen|5|False|LOJ
sitelen|6|False|E PIPI
sitelen|8|False|KASI
sitelen|9|False|JELO
sitelen|10|False|JELO LOJE A
sitelen|11|False|SELI TAWA
sitelen|12|False|TELO ANPA
sitelen|13|False|UTALA(SO)
sitelen|14|False|LASO WAWA
sitelen|15|False|WALO LOJE
sitelen|16|False|LOJE WALO
sitelen|17|False|LOJE PIPI

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
kalama|1|True|Kulu
kalama|2|True|Kulu
kalama|3|False|Kulu

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
kalama|1|True|Kulu
kalama|2|False|Kulu
kalama|3|True|Kulu
kalama|4|False|Kulu
kalama|5|True|Kulu
kalama|6|False|Kulu
kalama|20|False|m|nowhitespace
kalama|19|True|m|nowhitespace
kalama|18|True|M|nowhitespace
kalama|7|True|S|nowhitespace
kalama|8|True|U|nowhitespace
kalama|9|True|l|nowhitespace
kalama|10|False|upu !|nowhitespace
kalama|11|True|S|nowhitespace
kalama|12|True|A|nowhitespace
kalama|13|True|P|nowhitespace
kalama|14|True|a|nowhitespace
kalama|15|True|l|nowhitespace
kalama|16|True|a|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
kalama|1|True|P
kalama|2|True|ulu|nowhitespace
kalama|3|True|P
kalama|4|False|ulu|nowhitespace
kalama|7|True|u|nowhitespace
kalama|6|True|op|nowhitespace
kalama|5|True|l
kalama|10|True|u|nowhitespace
kalama|9|True|op|nowhitespace
kalama|8|True|l
kalama|13|False|u|nowhitespace
kalama|12|True|op|nowhitespace
kalama|11|True|l

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
mama|1|True|sitelen ni li nasin jo pi jan ale. jan 21 li pana e mani la, wan ni li lon.
mama|2|False|www.patreon.com/davidrevoy
mama|3|False|pona mute tan jan ni:
mama|4|False|pali kepeken ilo Krita lon ilo GNU/Linux
