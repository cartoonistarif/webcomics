# Transcript of Pepper&Carrot Episode 02 [la]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Episodium 2 : Potiones multicolores

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|5|True|Glut
Sound|6|True|Glut
Sound|7|False|Glut
Writing|1|True|PERICULOSA
Writing|3|False|NOLITE INTROIRE
Writing|2|True|MAGA
Writing|4|False|33

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|7|False|toc|nowhitespace
Writing|1|False|FIRE D
Writing|2|False|DEEP OCEAN
Writing|3|False|VIOLE(N)T
Writing|4|False|ULTRA BLUE
Writing|5|False|PIN
Writing|6|False|MSON
Writing|8|False|NATURE
Writing|9|False|YELLOW
Writing|10|False|ORANGE TOP
Writing|11|False|FIRE DANCE
Writing|12|False|DEEP OCEAN
Writing|13|False|VIOLE(N)T
Writing|14|False|ULTRA BLUE
Writing|15|False|META PINK
Writing|16|False|MAGENTA X
Writing|17|False|CRIMSON

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|Glut
Sound|2|True|Glut
Sound|3|False|Glut

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|Glut
Sound|2|False|Glut
Sound|3|True|Glut
Sound|4|False|Glut
Sound|5|True|Glut
Sound|6|False|Glut
Sound|20|False|m|nowhitespace
Sound|19|True|m|nowhitespace
Sound|18|True|M|nowhitespace
Sound|7|True|S|nowhitespace
Sound|8|True|P|nowhitespace
Sound|9|True|l|nowhitespace
Sound|10|False|urp !|nowhitespace
Sound|11|True|S|nowhitespace
Sound|12|True|S|nowhitespace
Sound|13|True|S|nowhitespace
Sound|14|True|P|nowhitespace
Sound|15|True|l|nowhitespace
Sound|16|True|o|nowhitespace
Sound|17|False|p|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|B
Sound|2|True|lup|nowhitespace
Sound|3|True|B
Sound|4|False|lup|nowhitespace
Sound|7|True|uts|nowhitespace
Sound|6|True|l|nowhitespace
Sound|5|True|p
Sound|10|True|ts|nowhitespace
Sound|9|True|lu|nowhitespace
Sound|8|True|p
Sound|13|False|ts|nowhitespace
Sound|12|True|lu|nowhitespace
Sound|11|True|p

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|True|Haec fabula imaginalis est aperta fontis et hoc episodium juverunt in
Credits|2|True|www.patreon.com/davidrevoy
Credits|3|False|21 maecenates. Gratias ago illis :
Credits|4|False|confectus Krita in GNU/Linux
