# Transcript of Pepper&Carrot Episode 02 [pl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Odcinek 2: Tęczowe eliksiry

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|5|True|plum
Sound|6|True|plum
Sound|7|False|plum
Writing|1|True|UWAGA!
Writing|3|False|WIEDŹMY
Writing|2|True|POSESJA
Writing|4|False|33

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|7|False|klank|nowhitespace
Writing|1|False|FIRE D
Writing|2|False|DEEP OCEAN
Writing|3|False|VIOLE(N)T
Writing|4|False|ULTRA BLUE
Writing|5|False|PIN
Writing|6|False|MSON
Writing|8|False|NATURE
Writing|9|False|YELLOW
Writing|10|False|ORANGE TOP
Writing|11|False|FIRE DANCE
Writing|12|False|DEEP OCEAN
Writing|13|False|VIOLE(N)T
Writing|14|False|ULTRA BLUE
Writing|15|False|META PINK
Writing|16|False|MAGENTA X
Writing|17|False|CRIMSON

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|plum
Sound|2|True|plum
Sound|3|False|plum

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|gul
Sound|2|False|gul
Sound|3|True|gul
Sound|4|False|gul
Sound|5|True|gul
Sound|6|False|gul
Sound|20|False|m|nowhitespace
Sound|19|True|m|nowhitespace
Sound|18|True|m|nowhitespace
Sound|7|True|t|nowhitespace
Sound|8|True|f|nowhitespace
Sound|9|True|u|nowhitespace
Sound|10|False|uuu !|nowhitespace
Sound|11|True|S|nowhitespace
Sound|12|True|S|nowhitespace
Sound|13|True|S|nowhitespace
Sound|14|True|P|nowhitespace
Sound|15|True|l|nowhitespace
Sound|16|True|a|nowhitespace
Sound|17|False|t|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|True|b
Sound|2|True|ul|nowhitespace
Sound|3|True|b
Sound|4|False|ul|nowhitespace
Sound|7|True|ap|nowhitespace
Sound|6|True|hl|nowhitespace
Sound|5|True|c
Sound|10|True|ap|nowhitespace
Sound|9|True|hl|nowhitespace
Sound|8|True|c
Sound|13|False|ap|nowhitespace
Sound|12|True|hl|nowhitespace
Sound|11|True|c

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|True|Ten webcomic jest na licencji open-source.Odcinek został ufundowany przez 21 patronów na:
Credits|2|False|www.patreon.com/davidrevoy
Credits|3|False|Lista zasłużonych:
Credits|4|False|zrobione z Krita na GNU/Linux
