# Transcript of Pepper&Carrot Episode 19 [de]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 19: Umweltverschmutzung

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|1|True|Ich gehe hinein.
Cayenne|2|True|Es bringt nichts, dir beim Graben zuzusehen …
Cayenne|3|False|Vergrabe all die misslungenen Zaubertränke und komm dann so schnell wie möglich rein, um Dich auszuruhen.
Cayenne|4|False|Es ist wohl nur ein frommer Wunsch, dass du damit morgen fertig bist – aber wer weiß?
Pepper|5|True|Okay, ich beeile mich ja schon!
Pepper|6|True|Übrigens, warum müssen wir alles vergraben ?
Pepper|7|False|Wäre es nicht besser, wenn wir...
Cayenne|8|False|Wenn WAS ?

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|labels in the garden :-)
Pepper|1|True|Ähm …
Pepper|2|True|also ich bin natürlich keine Expertin …
Pepper|3|True|… aber das Gemüsebeet ist schon sehr eigenartig dieses Jahr,
Pepper|4|False|und bei vielen anderen Pflanzen rund ums Haus sieht es ähnlich aus.
Schrift|5|False|Tomaten
Schrift|6|False|Auberginen
Pepper|7|False|Dasselbe mit den Ameisen, die treiben echt komische Sachen.
Pepper|8|True|Auf jeden Fall …
Pepper|9|False|Ich hab mir gedacht, wir haben vielleicht ein Umweltproblem, vielleicht sollten wir unser Zeug mal richtig entsorgen?
Cayenne|10|True|Hör zu, Fräulein „Ich-Ruiniere-All-Meine-Tränke“ ,
Cayenne|11|False|deine Hippiah-Verkleidung ist dir zweifellos zu Kopfe gestiegen.
Cayenne|12|True|In Chaosah vergraben wir unsere Fehler!
Cayenne|13|True|So ist es unsere Tradition seit Anbeginn der Zeit, und es kümmert mich 'nen Dreck, was MUTTER NATUR darüber denkt!
Cayenne|14|False|Also halt die Klappe und GRAB' !!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|wir vergraben unsere Fehler
Pepper|2|False|Tradition
Pepper|3|False|seit Anbeginn der Zeit
Pepper|4|False|NA KLAR!
Carrot|5|False|Zzzz
Pepper|6|False|Mach schon, schneller, Carrot!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|Diary label on book
Schrift|6|False|Cayennes Privates Tagebuch
Pepper|1|False|„Oh Du mein strahlender Held, so blond und goldgestählt!“
Pepper|2|False|„… Du weckst das Chaos in mir!“
Pepper|3|False|„… Glutvoll ist es verwirrt von Dir.“
Pepper|4|True|Schöner Reim, geliebte Cayenne!
Pepper|5|False|Schon verrückt, was man über Hexen rausfinden kann, die all ihre Fehler vergraben!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|1|True|Wir haben alle unsere Jugendsünden
Cayenne|2|True|Die Cayenne, die das geschrieben hat, gibt es heute nicht mehr.
Cayenne|3|False|Das Tagebuch war aus gutem Grund vergraben.
Pepper|4|False|...
Pepper|5|True|OK!
Pepper|6|False|Aber was sagst du dazu?
Schrift|7|False|CHAOSAH SUTRA von Thymian
Thymian|8|False|Nur eine jugendliche Verfehlung. Jedenfalls nichts für Dein Alter!
Pepper|9|True|Hmm …
Pepper|10|False|Ich verstehe …
Pepper|11|False|Euch ist wirklich gar nichts peinlich …

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Aber die Umwelt! Die Natur !
Pepper|2|False|Wir können nicht weiterhin alles so zumüllen, ohne dass das Folgen für uns hat!!!!
Cayenne|3|True|Es hat uns bisher keinerlei Probleme bereitet!
Cayenne|4|True|Wir sind Hexen von Chaosah! Und unsere Probleme werden
Cayenne|5|True|tief vergraben!
Cayenne|6|False|Und Traditionen werden nicht diskutiert!
Kümmel|7|True|Schaut mal, was ich gefunden habe!
Kümmel|8|False|Ich kann es gar nicht fassen!
Kümmel|9|False|Wie ist das hier runtergekommen?
Kümmel|10|False|Sie muss vielleicht ein wenig gestimmt werden, zupft sich aber noch ganz gut.

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|Label of materials to translate
Kümmel|1|False|Wie ging das gleich noch mal? Cha~Cha Cha, Chaooosah!
Kümmel|2|True|Haha, ich habe den Text vergessen!
Kümmel|3|False|Vielleicht ist mein Liederbuch ja auch noch irgendwo hier …
Cayenne|4|True|… also, wir sind uns alle einig. Neuerung bei den Regeln von Chaosah:
Cayenne|5|True|Von nun an
Cayenne|6|True|sortieren,
Cayenne|7|True|zerkleinern
Cayenne|8|True|und recyceln wir alles!
Cayenne|9|False|ALLES!!
Schrift|10|False|Glas
Schrift|11|False|Metall
Erzähler|12|False|- ENDE -
Impressum|13|False|09/2016 - www.peppercarrot.com - Grafik & Handlung : David Revoy - Deutsche Übersetzung : Julian Eric Ain
Impressum|14|False|Script doctor: Craig Maloney. Korrekturlesen und Unterstützung mit dem Dialog: Valvin, Seblediacre et Alex Gryson. Inspiration: "The book of secrets" von Juan José Segura
Impressum|15|False|Basierend auf dem Universum von Hereva von David Revoy mit Unterstützung von Craig Maloney. Korrekturen von Willem Sonke, Moini, Hali, CGand und Alex Gryson.
Impressum|16|False|Lizenz : Creative Commons Namensnennung 4.0, Software: Krita 3.0.1, Inkscape 0.91 auf Linux Mint Cinnamon

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Impressum|1|False|Pepper&Carrot is komplett frei, Open Source und wird durch die Leser unterstützt und finanziert. Für diese Episode geht der Dank an die 755 Förderer:
Impressum|2|False|Auch Du kannst Pepper&Carrot unterstützen: www.patreon.com/davidrevoy
