# Transcript of Pepper&Carrot Episode 31 [en]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Episode 31: The Fight

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrator|1|False|Tenebrume, sacred hill of Chaosah.
Sound|2|False|VRoo om !|nowhitespace
Pepper|3|False|Tsk!
Sound|4|False|VRoo oom|nowhitespace
Sound|5|False|Brzooo!
Pepper|6|False|Take that!
Sound|8|False|Schh!
Cayenne|9|False|Amateur!
Pepper|10|False|! !|nowhitespace
Sound|11|False|SLAM !|nowhitespace

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|False|Drzzooow!
Sound|2|False|CR ACK! !!|nowhitespace
Sound|3|False|CR A A CK ! !!|nowhitespace
Pepper|4|False|Not so fast!
Pepper|5|False|GRAVITATIONAS SHIELDUS!
Sound|6|False|Woooosh! !|nowhitespace
Sound|7|False|Tchkshkk! !|nowhitespace
Sound|8|False|Tchkshkk! !|nowhitespace
Sound|9|False|Tok!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|CARROT!
Pepper|2|False|Plan 7-B!
Pepper|4|False|JPEGUS QUALITIS!
Sound|5|False|Brz ooo !|nowhitespace
Sound|3|False|Zo o om|nowhitespace
Cayenne|6|False|?!!
Cayenne|10|False|Argh!!
Sound|7|True|G
Sound|8|True|Z|nowhitespace
Sound|9|False|Z|nowhitespace
Pepper|11|False|QUALITIS MINIMALIS!
Cayenne|12|False|! !|nowhitespace
Cayenne|13|False|Grr...
Writing|14|False|2019-12-20-E31P03_V15-final.jpg
Writing|15|False|Error loading
Sound|16|False|CRASH ! !!|nowhitespace

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|1|False|PYRO BOMBA ATOMICUS!
Sound|2|False|Frrzooow!
Sound|3|True|K
Sound|4|True|A|nowhitespace
Sound|5|True|B|nowhitespace
Sound|6|True|O|nowhitespace
Sound|7|True|O|nowhitespace
Sound|8|True|M|nowhitespace
Sound|9|True|!|nowhitespace
Sound|10|False|!|nowhitespace
Sound|11|True|BRRR
Sound|12|False|BRRR
Sound|13|False|P s hh h ...|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|1|False|Pfft...
Cayenne|2|True|Fleeing to a microdimension at your age?
Cayenne|3|False|What a pathetic defeat ...
Pepper|4|True|Wrong!
Pepper|5|False|A wormhole!
Sound|6|False|Bzz! !|nowhitespace
Pepper|7|False|Thanks for the exit, Carrot!
Pepper|8|False|Checkmate, master Cayenne!
Pepper|9|False|GURGES...
Pepper|10|False|...ATER!
Sound|12|False|Swwwwwwiiiiiiiipppp!!!
Sound|11|False|V R O OM ! !!|nowhitespace
Thyme|13|False|STOP!!!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Thyme|1|True|I said STOP!
Thyme|2|False|That's enough!
Sound|3|False|Snap !|nowhitespace
Sound|4|False|PO OOF !|nowhitespace
Sound|5|False|TCHK!
Thyme|6|True|Both of you!
Thyme|7|True|HERE!
Thyme|8|False|Immediately!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|False|Tok!
Sound|2|False|Tok!
Thyme|3|True|It's been three years since you told us "just a last-minute test"...
Thyme|4|False|...we're not going to spend the whole night here!
Thyme|5|False|So...?
Pepper|6|False|...
Cayenne|7|True|OK...
Cayenne|8|True|...she can have her degree...
Cayenne|9|False|...but at a "passable" level.
Writing|10|True|Degree
Writing|11|True|of
Writing|12|False|Chaosah
Writing|14|False|Cumin
Writing|13|False|Cayenne
Writing|15|False|T h yme|nowhitespace
Writing|16|False|~ for Pepper ~
Writing|17|False|Official Witch
Narrator|18|False|- FIN -

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|5|True|You too can become a patron of Pepper&Carrot and get your name here!
Pepper|3|True|Pepper&Carrot is entirely free(libre), open-source and sponsored thanks to the patronage of its readers.
Pepper|4|False|For this episode, thanks go to 971 patrons!
Pepper|7|True|Check www.peppercarrot.com for more info!
Pepper|6|True|We are on Patreon, Tipeee, PayPal, Liberapay ...and more!
Pepper|8|False|Thank you!
Pepper|2|True|Did you know?
Credits|1|False|December 20, 2019 Art & scenario: David Revoy. Beta readers: Craig Maloney, Martin Disch, Arlo James Barnes, Nicolas Artance, Valvin. English version Proofreading: Craig Maloney, Martin Disch . Based on the universe of Hereva Creator: David Revoy. Lead maintainer: Craig Maloney. Writers: Craig Maloney, Nartance, Scribblemaniac, Valvin. Correctors: Willem Sonke, Moini, Hali, CGand, Alex Gryson . Software: Krita 4.2.6appimage, Inkscape 0.92.3 on Kubuntu 18.04-LTS. License: Creative Commons Attribution 4.0. www.peppercarrot.com
<hidden>|0|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|0|False|You can also translate this page if you want.
<hidden>|0|False|Beta readers help with the story, proofreaders give feedback about the text.
