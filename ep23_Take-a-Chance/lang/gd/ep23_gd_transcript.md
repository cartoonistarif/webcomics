# Transcript of Pepper&Carrot Episode 23 [gd]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tiotal|1|False|Eapasod 23: Cothrom na Fèinne

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Neach-aithris|1|False|Fèill Chomona an ath-sheachdain
Sgrìobhte|2|False|Cliùiteachd
Sgrìobhte|3|False|50 000 Co
Sgrìobhte|4|False|Buaidh Chròch
Sgrìobhte|5|False|Fasan
Sgrìobhte|6|False|Sealladh air Cròch
Sgrìobhte|7|False|Snas
Sgrìobhte|8|False|Cròch fon phrosbaig
Fuaim|9|False|POC !|nowhitespace

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|1|False|Tapadh leat, a Churrain…
Peabar|2|False|Fhios a’d, tha mi ag obair gu cruaidh gach latha ach an rachainn ’nam dheagh bhana-bhuidseach...
Peabar|3|True|...le spèis air an dualchas is air na riaghailtean...
Peabar|4|False|...chan eil e cothromach gun do choisinn Cròch uiread dhe chliù leis an ro-innleachd a bha seo.
Peabar|5|False|Abair ana-cothrom.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Jinn an t-soirbheis|2|True|Haoidh!
Jinn an t-soirbheis|3|False|Le ’r leisgeul, innsim dhuibh mu m’ dhèidhinn:
Fuaim|1|False|Glung !|nowhitespace
Jinn an t-soirbheis|4|False|Is mise jinn an t-soirbheis, air mhuinntireas dhuibh fad oidhche ’s latha ’s a h-uile latha dhen t-seachdain!*

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Jinn an t-soirbheis|1|False|*Seach deiridhean-seachdain agus saor-làithean poblach. Chan fhaodar an tairgse a chur còmhla ri tairgsean eile. Tha an fhaotainneachd cuingichte a-rèir stoca.
Jinn an t-soirbheis|2|False|Iomairt margaidh-eachd sa bhad air feadh an t-saoghail!
Jinn an t-soirbheis|3|False|Cliù eadar-nàiseanta!
Jinn an t-soirbheis|4|False|A’ gabhail a-staigh stoidhlichean is gruagairean!
Jinn an t-soirbheis|5|False|Barantas air toraidhean ann am priobadh na sùla!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Jinn an t-soirbheis|1|False|Chan eil a dhìth ach ainm-sgrìobhte beag bìodach aig bonn na sgrìobhainne seo agus fosglaidh doras a’ chliùtha dhuibh!
Peabar|2|False|Fuirich diog.
Peabar|4|True|Dè tha air cùl an Deus ex machina a tha seo?!
Peabar|3|False|An ann air an dearbh uair a bhios an lionn-dubh orm gu dona gun tuit thu às na speuran le fuasgladh mìorbhaileach air na duilgheadasan agam uile?!
Peabar|5|False|Dhomhsa dheth, tha coltas amharasach air; glè amharasach!
Peabar|6|True|Trobhad, a Churrain.
Peabar|7|False|’S dòcha gun obraicheadh a leithid a char seo oirnn uair ach chan eil sinn cho gòrach tuilleadh ’s gun gabhadh ar mealladh leis an fhoill seo.

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Eun|1|True|Gag!
Eun|2|True|Gurr-ù!
Eun|3|True|Gurr-ù!
Eun|4|False|Gag!
Peabar|5|False|Nise a Churrain, seo nas ciall dha thighinn gu h-inbhe nam bheachd-sa.
Peabar|6|False|Seachain am fuasgladh furasta; thoir meas air luach na spàirne...
Peabar|7|False|...chan eil ciall sam bith san fharmad air soirbheis chàich!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sgrìobhte|1|False|Cliùiteachd
Sgrìobhte|2|False|50 000 Co
Sgrìobhte|3|False|Buaidh Chròch
Sgrìobhte|4|False|Fasan
Sgrìobhte|5|False|Sealladh air Cròch
Peabar|8|False|Thig an latha a gheibh sinn cothrom na Fèinne!
Sgrìobhte|6|False|Snas
Sgrìobhte|7|False|Cròch fon phrosbaig
Fuaim|9|False|Psiuuu !
Sgrìobhte|10|False|Cliùiteachd
Sgrìobhte|11|False|Caitheamh-beatha Ghuragag
Sgrìobhte|12|False|Fasan
Sgrìobhte|13|False|Sealladh air Guragag
Sgrìobhte|14|False|Snas
Sgrìobhte|15|False|Guragag fon phrosbaig
Neach-aithris|16|False|- Deireadh na sgeòil -

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Urram|1|False|08/2017 – www.peppercarrot.com – Obair-ealain ⁊ sgeulachd: David Revoy – Eadar-theangachadh: GunChleoc
Urram|2|False|Ceartachadh ’s piseach air na còmhraidhean: Alex Gryson, Calimeroteknik, Nicolas Artance, Valvin ’s Craig Maloney.
Urram|4|False|Stèidhichte air saoghal Hereva a chaidh a chruthachadh le David Revoy le taic o Craig Maloney. Ceartachadh le Willem Sonke, Moini, Hali, CGand ’s Alex Gryson.
Urram|5|False|Bathar-bog: Krita 3.1.4, Inkscape 0.92dev air Linux Mint 18.2 Cinnamon
Urram|6|False|Ceadachas: Creative Commons Attribution 4.0
Urram|8|False|’S urrainn dhut dol ’nad phàtran Pepper&Carrot cuideachd air www.patreon.com/davidrevoy
Urram|7|False|Tha Pepper&Carrot saor is le tùs fosgailte air fad agus ’ga sponsaireadh le taic nan leughadairean. Mòran taing dhan 879 pàtran a thug taic dhan eapasod seo:
Urram|3|False|Taic leis a’ bhòrd-stòiridh ’s mise-en-scène: Calimeroteknik ’s Craig Maloney.
