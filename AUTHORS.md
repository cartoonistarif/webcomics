Pepper&Carrot contributors
==========================

## Artwork

* [David Revoy](https://framagit.org/Deevad), founder of [www.peppercarrot.com](https://www.peppercarrot.com/)

## Translations

* Arabic: Mahwiii

* Bengali: Hulo Biral the Tomcat

* Breton: Stefan Carpentier

* Catalan: Juan José Segura

* Chinese: Ran Zhuang, Yan Jing, Lou Simons

* Czech: Kateřina Fleknová, Martin Doucha

* Danish: [Emmiline Alapetite](https://github.com/Emmilinette), [Alexandre Alapetite](https://github.com/Alkarex), Rikke Skaaning Alapetite, Marie Moestrup, Juan José Segura

* Dutch: [Willem Sonke](https://github.com/Willem3141), [Midgard](https://framagit.org/Midgard), Peter Moonen

* English: [Alex Gryson](https://framagit.org/agryson), [David Revoy](https://framagit.org/Deevad), Amireti

* Esperanto: [sitelen](https://librefan.eu.org), Kaoseto, Naj Ledofiŝ, Yanou, Spenĉjo, Gruiber, chessnerd Jason, Jorge Maldonado Ventura, Ionicero, Tirifto

* Farsi: Aslan Zamanloo

* Filipino: Paolo Abes

* Finnish: Kari Lehto

* French: [David Revoy](https://framagit.org/Deevad), Aurélien Gâteau, Remi Rampin, Xavier Dolques, [Nicolas Artance](https://framagit.org/Nartance), Valvin, Calimeroteknik

* Gallo: Patrik Deriano, Gwenaelle Lefeuvre

* German: Alexandra Jordan, Helmar Suschka, Carlo Gandolfi, colognella, Julian Eric Ain, Philipp (hapit) Hemmer, Ret Samys, [Martin Disch](https://framagit.org/martindisch)

* Greek: Dimitris Axiotis, George Karettas

* Hungarian: Halász Gábor "Hali", [StarWish](https://starwish.hu/)

* Ido: Gilles-Philippe Morin, Brian E. Drake, William Johnsson

* Indonesian: Bonaventura Aditya Perdana, Reinaldo Calvin

* Italian: Carlo Gandolfi, corrections: Antonio Parisi

* Japanese: guruguru

* Korean: Jihoon Kim, Shikamaru "initbar" Yamamoto

* Kotava: Erwhann-Rouge, webmistusik, Mardixuran, [Kotava dokalixo](http://kotava.org/phpBB3/index.php)

* Latin: Guillaume Lestringant

* Lojban: [la gleki (ly. Gleki Arxokuna ly.)](https://github.com/lagleki), lai krtis, mezohe

* Low German: Rebecca Breu

* Malay: [muhdnurhidayat](https://mnh48.moe)

* Malayalam: [Pranav Jerry](https://gitlab.com/gitinator)

* Mexicano: [R J Quiralta](https://github.com/RJQuiralta)

* Norman: [Julien R. Mauduit](https://framagit.org/RouogeFale)
 
* Norwegian Bokmål: Thomas Nordstrøm

* Norwegian Nynorsk: Arild Torvund Olsen, [Karl Ove Hufthammer](https://framagit.org/huftis)

* Polish: Przemysław "Kev" Chudzia, Sölve Svartskogen

* Portuguese: [Alexandre E. Almeida](https://framagit.org/almeidaxan), Frederico Batista

* Romanian: Costin-Sorin Ionescu

* Russian: Andrew "Akari" Alexeyew, [Denis "uncle Night" Prisukhin](https://github.com/uncleNight), [Gleki "lagleki" Arxokuna](https://github.com/lagleki), [Zveryok](https://framagit.org/Zveryok), Mishvanda. 

* Serbian: Marko Ivancevic

* Silesian: Grzegorz Kulik

* Sinhala: Tharinda Divakara

* Slovak: talime

* Slovene: Andrej Ficko

* Spanish: [TheFaico](https://github.com/TheFaico)

* Swedish: Mikael "Quiphius" Olofsson, Quiphius

* Ukrainian: [Denis "uncle Night" Prisukhin](https://github.com/uncleNight), Leonid "Deburger" Androschuk, [Zveryok](https://framagit.org/Zveryok)

* Vietnamese: Binh Pham, Hồ Châu, Phiên bản Tiếng Việt

## Developers

- Mjtalkiewicz: multithreading of the renderfarm
