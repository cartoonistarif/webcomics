# Transcript of Pepper&Carrot Episode 26 [fr]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titre|1|False|Épisode 26 : Les livres, c'est génial

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Les livres, c'est génial !
Pepper|3|False|C'est une pure merveille !
Pepper|2|True|Ce livre rare contient les notes d'un aventurier chevronné, et décrit tout dans ce donjon.
Pepper|4|False|Par exemple, l'œil maléfique de la porte principale lance des boules de feu pour repousser les intrus.
Pepper|5|False|Cependant, si on s'approche par le côté, hors de vue…

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|…et qu'on couvre l'œil avec un chapeau…
Pepper|3|False|…la porte devrait s'ouvrir d'elle-même !
Pepper|4|True|Ils conseillent même de laisser le chapeau là pour s'échapper facilement !
Pepper|5|False|Les livres, c'est tellement génial !
Écriture|6|False|« Sautez tout de suite par-dessus le premier balcon. »
Pepper|7|False|OK !
Son|9|False|BOING
Son|10|False|BOING
Écriture|8|False|« Vous noterez une sensation de chute. C'est normal. C'est un raccourci. »
Écriture|11|False|« Profitez du confort de la toile illuminée sans aucun risque : ces araignées photosynthétiques se nourrissent exclusivement de la lumière qu'elle reflète. »
Écriture|12|False|« Je recommande une sieste rapide pour reprendre toutes vos forces. »
Pepper|13|False|J'adore les livres !
Son|2|False|Pouf !

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|2|False|Je sais combien les chats détestent être mouillés.
Pepper|3|True|T'inquiète pas !
Pepper|4|False|Les livres sont aussi utiles pour ça !
Pepper|5|True|Et moins de dix minutes après être entrée dans le donjon, me voilà dans la salle du trésor à cueillir les feuilles magiques du fameux Arbre d'Eau.
Pepper|6|False|Les livres, c'est magique !
Pepper|1|True|Oh, Carrot…

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Écriture|3|False|« Comme vous avez pris un rac-courci, vous devriez arriver à la salle du Gardien. »
Pepper|4|True|Hmm…
Pepper|5|True|Le boss final ?!
Pepper|6|False|Mais où ?
Pepper|7|True|AH-HA !
Pepper|8|False|Trouvé !
Pepper|11|True|Pffff... !
Pepper|12|False|Trop facile quand on connaît le point faible !
Écriture|1|False|« Ramassez une plume en passant. »
Pepper|2|False|Ça ne manque pas de plumes, par ici !
Pepper|9|True|guili
Pepper|10|False|guili

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|1|False|Crrrr !|nowhitespace
Pepper|2|False|Aha ! Voilà la sortie !
Son|3|False|Wooop !
Pepper|5|True|Beeerk…
Pepper|6|False|Je suis sûre qu'il y a des instructions pour retirer cette « chose »…
Pepper|7|True|Carrot… tu peux me lire ça ?
Pepper|8|True|Heu… évidemment que non…
Pepper|9|False|Pff…
Pepper|4|False|?!
Pepper|12|False|Grrrr !!!
Pepper|10|True|JE LE SAVAIS !!!
Pepper|11|True|C'était trop beau pour être vrai !
Pepper|13|False|Peut-être que les livres ne peuvent pas tout régler, en fait…
Son|14|False|Paf !

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|…à moins que…
Pepper|2|False|YAA A A A A A A ! ! !|nowhitespace
Son|3|False|PAF !|nowhitespace
Pepper|5|False|…les livres, c'est génial !
Pepper|4|True|Eh non en fait, dans toutes les situations…
Narrateur|6|False|- FIN -

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crédits|3|False|Vous aussi, devenez mécène de Pepper&Carrot sur www.patreon.com/davidrevoy
Crédits|2|False|Pepper&Carrot est entièrement libre, open-source, et sponsorisé grâce au mécénat de ses lecteurs. Pour cet épisode, merci aux 1098 mécènes :
Crédits|1|False|Licence : Creative Commons Attribution 4.0. Logiciels : Krita 4.1, Inkscape 0.92.3 sur Kubuntu 17.10. Dessin & Scénario : David Revoy. Script doctor : Craig Maloney. Correcteurs : Craig Maloney, CalimeroTeknik. Bêta-lecteurs : Ambernite, Craig Maloney, CalimeroTeknik, Jookia, Midgard, Nartance, Popolon, Quiralta, Valvin, xHire, Zeograd. Version Française Traducteurs : Valvin, CalimeroTeknik. Correcteurs : Nicolas Artance, David Revoy. Basé sur l'univers d'Hereva Creation : David Revoy. Mainteneur : Craig Maloney. Éditeurs : Craig Maloney, Nartance, Scribblemaniac, Valvin. Correcteurs : Willem Sonke, Moini, Hali, CGand, Alex Gryson. 28 juillet 2018 www.peppercarrot.com
<hidden>|0|False|Remove the names of the people who helped to create the current version and create this section for your language. You can credit translators, proofreaders, whatever categories you want. N.B.: In Inkscape, if you press Enter to create a new line, it will not push down text below it until the line has text. So to create a whiteline, type a space on its own line.
