# Transcript of Pepper&Carrot Episode 26 [gd]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tiotal|1|False|Eapasod 26: Comhairle nan leabhraichean

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|1|False|’S caomh leam leabhraichean!
Peabar|3|False|Nach math sin!
Peabar|2|True|Tha nòtaichean taistealaich chomasaich san leabhar ainneamh seo a mhìnicheas a h-uile rud mun dùn seo.
Peabar|4|False|Mar eisimpleir: losgaidh droch-shùil an dorais seo bàlaichean-teine marbhtach a chumas aoighean gun chuireadh air falbh.
Peabar|5|False|Ach ma theannas tu oirre gu cùramach on chliathach far nach fhaic i thu...

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|1|False|...’s ma chòmhdaicheas tu an t-sùil le ad...
Peabar|3|False|...fosglaidh an doras leis fhèin an uairsin!
Peabar|4|True|Tha fiù ’s moladh ann gum fàgamaid an ad far a bheil e ach an teicheamaid gun duilgheadas an uairsin!
Peabar|5|False|Tha deagh chomhairle san leabhar seo!
Sgrìobhte|6|False|“Thoir leum thar a’ chiad rèile.”
Peabar|7|False|Ceart ma-thà!
Fuaim|9|False|BOC
Fuaim|10|False|BOC
Sgrìobhte|8|False|“Dh’fhaoidte gum fairich thu do thuiteam. Na gabh dragh dheth. Seo ath-ghoirid.”
Sgrìobhte|11|False|“Gabh tlachd à cofhurtachd an lìn shoillsichte gun chunnart: cleachdaidh na damhain-allaidh seo co-thàthadh ’s cha ghabh iad ach ath-thilgeadh an t-solais.”
Sgrìobhte|12|False|“Mholainn-sa norrag airson do neartachadh.”
Peabar|13|False|’S fhìor chaomh leam leabhraichean!
Fuaim|2|False|Ploc!
<hidden>|0|False|This “they” means one person, without specifying their gender. If possible, please try to do the same in your language (suggestion: if your language doesn't have genderless pronouns, maybe you can translate as “the author”). If that doesn't really work well, assume the author is a woman.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|2|False|Tha fhios a’m gur lugha air cait a bhith fliuch.
Peabar|3|True|Na gabh dragh!
Peabar|4|False|Tha leabhraichean feumail airson sin cuideachd!
Peabar|5|True|’S cha dug e deich mionaidean o thighinn a-steach dhan dùn, ’s mi ’nam shuidhe ann an seòmar ulaidh a’ buain duilleagan on chraobh-uisge ainneamh mu thràth.
Peabar|6|False|Tha leabhraichean mìorbhaileach!
Peabar|1|True|Ò, a Churrain;

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sgrìobhte|3|False|“On a ghabh thu an ath-ghoirid, bu chòir dhut teannadh air seòmar an Fhreiceadain.”
Peabar|4|True|An-dà...
Peabar|5|True|an ceannard mòr?!?
Peabar|6|False|Ach càite?
Peabar|7|True|A HÀ...
Peabar|11|True|Pfff...!
Peabar|12|False|Ro fhurasta nuair a bhios tu eòlach air an easbhaidh!
Sgrìobhte|1|False|“Tog ite air an t-slighe.”
Peabar|2|False|Chan eil gainnead dhe dh’itean an-seo!
Fuaim|9|True|ciogail
Fuaim|10|False|ciogail
Peabar|8|False|Lorg mi thu!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fuaim|1|False|Chrrcrr !|nowhitespace
Peabar|2|False|Ahà! Seo an doras a-mach!
Fuaim|3|False|Saidse!
Peabar|6|False|Tha mi cinnteach gu bheil comhairle san leabhar air mar a gheibh thu cuidhteas dhen “nì” a tha seo...
Peabar|7|True|A Churrain... an leugh thu e?
Peabar|8|True|An-dà... chan eil an comas seo agad...
Peabar|9|False|Ochan...
Peabar|4|False|?!
Peabar|10|True|Grrr!!!
Peabar|11|True|BHA DÙIL AGAM RIS!!!
Peabar|12|False|’S an turas seo ro fhurasta!
Peabar|13|False|Air deireadh na sgeòil, ’s dòcha nach fhoghainn leabhar anns gach suidheachadh...
Fuaim|14|False|Paf!
Peabar|5|True|Ceachhh...

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|1|False|...ach ma...
Peabar|2|False|ÈAA A A A A A A ! ! !|nowhitespace
Fuaim|3|False|Pais !|nowhitespace
Peabar|5|False|...gur fìor chaomh leam leabhraichean!
Peabar|4|True|’S e an fhìrinn ann an suidheachadh sam bith...
Neach-aithris|6|False|- Deireadh na sgeòil -

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Urram|1|False|Ceadachas: Creative Commons Attribution 4.0. Bathar-bog: Krita 4.1, Inkscape 0.92.3 air Kubuntu 17.10. Obair-ealain ⁊ sgeulachd: David Revoy. Plodraigeadh sgriobtaichean: Craig Maloney. Leughadairean Beta: Ambernite, Craig Maloney, CalimeroTeknik, Jookia, Midgard, Nartance, Popolon, Quiralta, Valvin, xHire, Zeograd. Tionndadh Gàidhlig Eadar-theangachadh: GunChleoc. Stèidhichte air saoghal Hereva Air a chruthachadh le: David Revoy. Prìomh neach-glèidhidh: Craig Maloney. Sgrìobhadairean: Craig Maloney, Nartance, Scribblemaniac, Valvin. Ceartachadh: Willem Sonke, Moini, Hali, CGand, Alex Gryson. 28mh dhen Iuchar 2018 www.peppercarrot.com
Urram|3|False|’S urrainn dhut dol ’nad phàtran Pepper&Carrot cuideachd air www.patreon.com/davidrevoy
Urram|2|False|Tha Pepper&Carrot saor is le tùs fosgailte air fad agus ’ga sponsaireadh le taic nan leughadairean. Mòran taing dhan 1098 pàtran a thug taic dhan eapasod seo:
<hidden>|0|False|Remove the names of the people who helped to create the English version and create this section for your language. You can credit translators, proofreaders, whatever categories you want. N.B.: In Inkscape, if you press Enter to create a new line, it will not push down text below it until the line has text. So to create a whiteline, type a space on its own line.
