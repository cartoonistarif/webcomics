# Transcript of Pepper&Carrot Episode 11 [en]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Episode 11: The Witches of Chaosah

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|1|False|Pepper, you bring shame to Chaosah.
Thyme|2|False|Cayenne is right, a witch of Chaosah worth the title must command fear, obedience and respect.
Cumin|3|False|... while you, you offer tea and cupcakes, even to our demons*...
Note|4|False|* See episode 8: Pepper's Birthday Party.
Pepper|5|True|But...
Pepper|6|False|...godmothers...

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|4|False|SILENCE !
Cumin|9|False|Ta-daaa!
Thyme|1|True|Pepper, you are certainly talented, but you are also our only successor.
Thyme|2|False|It is our duty to make you a true, mean witch of Chaosah.
Pepper|3|False|But... I don't want to be mean! It's ...it's contrary to everyth...
Cayenne|5|True|... or we'll revoke all of your powers!
Cayenne|6|False|And you'll once more become the little idiot orphan of Squirrel's End.
Thyme|7|False|Cumin will shadow you from now on to help train you and update us with your progress.
Sound|8|False|Poof!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cumin|1|False|The new King of Acren is too kind and gentle with his subjects. The people must fear their King.
Cumin|2|False|Your first mission is to find and intimidate the monarch to better manipulate him.
Cumin|3|False|A true witch of Chaosah has influence over the powerful!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|The King ?...
Pepper|2|False|...so young ?!
Pepper|3|False|We must be the same age...
Cumin|4|False|What are you waiting for?! Go on! Wake him, intimidate him, scare him!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|False|Dzzz
Pepper|2|False|You and I, we are a little alike...
Pepper|3|True|young...
Pepper|4|True|alone...
Pepper|5|False|...prisoners of our destinies.

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Thyme|6|False|We've got a lot of work ahead of us Pepper...
Credits|8|False|09/2015 - Art & Scenario: David Revoy - Translation: Alex Gryson
Narrator|7|False|- FIN -
Sound|1|False|Poof!
Pepper|2|False|?!
Sound|3|False|Floof !
Thyme|4|True|First test:
Thyme|5|True|TOTAL FAILURE!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Pepper&Carrot is entirely free(libre), open-source and sponsored thanks to the patronage of its readers. For this episode, thank you to the 502 Patrons:
Credits|3|False|https://www.patreon.com/davidrevoy
Credits|2|True|You too can become a patron of Pepper&Carrot for the next episode at
Credits|4|False|License : Creative Commons Attribution 4.0 Source : available at www.peppercarrot.com Software : this episode was 100% drawn with libre software Krita 2.9.7, G'MIC 1.6.5.2, Inkscape 0.91 on Linux Mint 17
