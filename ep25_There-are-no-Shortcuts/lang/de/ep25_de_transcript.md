# Transcript of Pepper&Carrot Episode 25 [de]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 25: Es gibt keine Abkürzungen

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Schrift|1|False|Katzenfutter

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Erzähler|13|False|- ENDE -
Pepper|1|True|?
Pepper|2|True|?
Pepper|3|True|?
Pepper|4|False|?
Pepper|5|True|?
Pepper|6|True|?
Pepper|7|True|?
Pepper|8|False|?
Pepper|9|True|?
Pepper|10|True|?
Pepper|11|True|?
Pepper|12|False|?

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Impressum|7|False|Auch du kannst Pepper&Carrot unterstützen: www.patreon.com/davidrevoy
Impressum|6|False|Pepper&Carrot ist komplett frei, Open Source und wird durch die Leser unterstützt und finanziert. Für diese Episode geht der Dank an 810 Förderer:
Impressum|1|False|05/2018 - www.peppercarrot.com - Illustration & Handlung: David Revoy
Impressum|3|False|Basierend auf der Hereva-Welt von David Revoy mit Beiträgen von Craig Maloney. Korrekturen von Willem Sonke, Moini, Hali, CGand und Alex Gryson.
Impressum|4|False|Software: Krita 4.0.0, Inkscape 0.92.3 auf Kubuntu 17.10
Impressum|5|False|Lizenz: Creative Commons Namensnennung 4.0
Impressum|2|False|Beta-Feedback: Nicolas Artance, Imsesaok, Craig Maloney, Midgard, Valvin, xHire und Zveryok.
