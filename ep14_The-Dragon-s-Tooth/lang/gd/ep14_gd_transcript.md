# Transcript of Pepper&Carrot Episode 14 [gd]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tiotal|1|False|Eapasod 14: An fhiacaill-dhràgoin

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Carabhaidh|6|True|Uill uill uill! Tha na saor-làithean seachad!
Carabhaidh|7|False|Tòisicheamaid le cuspair nan deochan àrsaidh ’s nam prìomh-chungaidhean aca.
Carabhaidh|8|True|An-dà... Gonadh! Chan eil fiacaill-dhràgoin fhùdarach air fhàgail.
Carabhaidh|9|False|Chan fhiach tòiseachadh air a’ chuspair às a h-aonais ...
Sgrìobhte|10|False|Fiacaill-dhràgoin
Sgrìobhte|5|False|33
Sgrìobhte|2|True|TAIGH
Sgrìobhte|1|True|RABHADH
Sgrìobhte|3|True|BANA-
Sgrìobhte|4|False|BHUISDICH|nowhitespace

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Carabhaidh|1|False|A Chìob! A Thìom! An dèid tè agaibh an tòir air fiacaill-dhràgoin dhomh?
Cìob|2|False|Fhad ’s a tha i cho fuar ris a’ phuinnsean?! Cha dèid!
Tìom|3|False|Cha dèid no mise ...
Peabar|4|False|Air ur socair, gheibh mise tè dhuibh!
Peabar|6|False|Cha doir seo ach greiseag!
Peabar|7|True|“Cho fuar”, “cho fuar”!
Peabar|8|False|Abair fannlagan!
Peabar|9|False|Le greimire math ’s beagan armachd, bidh fiaclan-dràgoin againn gun dàil!
Carabhaidh|5|False|A Pheabar, fuirich ort!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|1|True|Hà hà!
Peabar|2|False|Hò-rò ! Cha chreid mi nach bi e beagan nas toinnte na shaoilinn!
Peabar|3|True|Path !
Peabar|4|False|.. ’s mi dhen bheachd gum biodh e na b’ fhasa le dràgon adhair!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|1|True|A Churrain!
Peabar|2|True|Trobhad a-nis ...
Peabar|3|True|... Tha dràgonan boglaich cliùiteach air sàilleibh an nàdair sholta!
Peabar|4|False|seo na shaoil mi ge-tà...
Peabar|5|True|An-dà ... A thaobh an dràgoin dhealain ...
Peabar|6|False|... Chan eil mi cinnteach gur e seo an acainn cheart ...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|1|True|Ceart ma-thà.
Peabar|2|True|Tha seo ro dhoirbh ...
Peabar|3|False|... ’s leigidh mi romham e
Peabar|5|False|Chan eil Peabar na gealltair !
Peabar|4|True|Cha leig !
Eun|7|False|GOG-a-ghuidhe-GhAOIdhe ! ! !|nowhitespace
Neach-aithris|6|False|An làrna-mhàireach...
Carabhaidh|8|False|! !|nowhitespace

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Neach-aithris|10|False|- Deireadh na sgeòil -
Urram|11|False|01/2016 – Obair-ealain ⁊ sgeulachd: David Revoy – Eadar-theangachadh: GunChleoc
Sgrìobhte|6|False|dràgoin|nowhitespace
Sgrìobhte|5|True|-|nowhitespace
Sgrìobhte|4|True|Fiaclaire
Sgrìobhte|7|False|An-asgaidh!
Fuaim|3|False|brag !|nowhitespace
Peabar|1|True|Dè ur beachd? Nach eil sibh moiteil asam?
Peabar|2|False|Chruinnich mi còrr is ceud fhiacaill-dhràgoin mu thràth!
Carabhaidh|8|True|... ach a Pheabar, an fhiacaill-dhràgoin ...
Carabhaidh|9|False|... ’S e lus a th’ ann !

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Urram|1|False|Tha Pepper&Carrot saor is le tùs fosgailte air fad agus ’ga sponsaireadh le taic nan leughadairean. Mòran taing dhan 671 pàtran a thug taic dhan eapasod seo:
Urram|2|True|’S urrainn dhut dol ’nad phàtran Pepper&Carrot cuideachd airson an ath-eapasod air
Urram|3|False|https://www.patreon.com/davidrevoy
Urram|4|False|Ceadachas: Creative Commons Attribution 4.0 Bun-tùs: ri fhaighinn air www.peppercarrot.com Bathar-bog: chaidh 100% dhen eapasod seo a tharraing le bathar-bog saor Krita 2.9.10, Inkscape 0.91 air Linux Mint 17
