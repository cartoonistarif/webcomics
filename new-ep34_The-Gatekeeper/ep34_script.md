# WIP: Episode 34: The interview (v2)

- [Contributopia](https://www.peppercarrot.com/0_sources/0ther/misc/low-res/2017-10-09_framasoft-campaign_3_Educ-Pop_by-David-Revoy.jpg): ideas for the land of Ah colors.
- [First depiction of the land Ah](https://www.peppercarrot.com/0_sources/ep08_Pepper-s-Birthday-Party/hi-res/gfx-only/gfx_Pepper-and-Carrot_by-David-Revoy_E08P02.jpg) in episode 8.
- [Schichimi in front of the temple](https://www.peppercarrot.com/0_sources/0ther/misc/hi-res/2016-06-06_environment-shichimi_ep17_livestream_by-David-Revoy.jpg) first concept-art
- [More temples](https://www.peppercarrot.com/0_sources/ep17_A-Fresh-Start/hi-res/gfx-only/gfx_Pepper-and-Carrot_by-David-Revoy_E17P06.jpg) on landscape of episode 17
- [A more recent artwork of Shichimi](https://www.peppercarrot.com/0_sources/0ther/artworks/hi-res/2019-10-19_Shichimi_by-David-Revoy.jpg) with a less naive setting for the land of Ah, maybe a bit more near to what I have in mind for painting the main temple.
- [First appearance of Wasabi](https://www.peppercarrot.com/0_sources/ep28_The-Festivities/hi-res/gfx-only/gfx_Pepper-and-Carrot_by-David-Revoy_E28P05.jpg) invited at the coronation of Coriander.

_Second rewrite around a [dialog sent by Craig Maloney](https://framagit.org/peppercarrot/webcomics/-/issues/167#note_752153)._

Large panorama on the land of Ah at advanced dusk. Beautiful stars, 3 crescents of moons. In the foreground, a campfire, a tent and two silhouettes are talking.

> Narrator: Land of the setting moons

Full Shot on the campfire, it's Pepper with a smoky bowl of soup and Shichimi who still mix the cauldron.

> Pepper: So that's how Carrot and I managed to get away.  
> Shichimi: Wow, that's an amazing tale.

Close-up on Pepper mixed between frustration, humble and optimism.

> Pepper: Yes, but that means I'm out of a job again

Close-up on Shichimi softly smiling to Pepper while serving a bowl of soup.

> Shichimi: I think my master might be able to help.  
>           She keeps saying we have more work than students,  
>           so maybe she could use some help  

Large mini-panel from far away the campfire, closing the sequence.

> Pepper: Thank you, Shichimi. It's worth a shot


---

Large panorama on the huge main temple of Ah: a very special ancient building made mainly of white bones of Dragons. The landscape around looks like a well maintained park, with it's geometric shaped trees, cute waterfalls, patch of perfect grass and floating island. It also has something of a camping: many triangular tents are installed around with small campfire. A long, long stairway of white stones goes to to the temple and many path around the temple connects to the base of the stair. Around the temple, you can see the daily life of hundreds of silhouettes. Many going out, going in, doing task around the place (sport like Tai chi, gardening, meditation in front of their tent). Most of the people around are dressed in the same style as Shichimi; they have long kimonos and tools and decorations are made of bones.

> Narrator: The day after at the temple of Ah

Large view inside a dark room we presume to be inside the building. The room has a sleek design: not a single object or pattern or decoration: only white smooth marble surfaces. A large rectangular desk stands in the middle of the room in front of a wide thick curtain that let filter only a single ray of light that paint a diagonal of strong light inside the picture. Shapes of massive white bones of dragons decorate the room as tribal sculptures. The impossible triangle of Chaos and Evolution is outlined on the curtain, oriented like a pyramid: Ah being on top of the triangle. Behind the desk, a tiny silhouette, Wasabi (the leader of Ah magic) do paper work: papers all over the place (but well organized). She is a young woman dressed with a white kimono. She has a pale yellow-green haircut and six black bones polished and glossy in her haircut. There is nowhere for Pepper to sit. She stands before Wasabi. Shichimi interrupts Wasabi.

> Shichimi: Master Wasabi, may I introduce my friend ...

High angle shot on Wasabi (POV from Shichimi or Pepper) interrupting her paper work. Wasabi looks up with a withering look

> Wasabi: Shichimi, I told you I'm not to be disturbed right now.
>         Be useful and get me the latest Hippiah reports

---

Medium shot low-angle (POV Wasabi) on Shichimi, suddenly a bit under panic. She is shy and this type of sentence has a big effect on her.

> Shichimi: Yes, Ma'am!

Medium long shot on Pepper on left of the picture and Shichimi going quickly to the door doing sign to her friend with her hands she is sorry she has to leave. Pepper is confused.

Low angle Full shot on the room where Pepper still stands, probably waiting Wasabi to do something. But this one work, deep down on her paper. An awkward minute pass.

Medium shot on Pepper; looking somewhere on right.

Medium shot on Pepper; looking somewhere on left.

Shot on Pepper finally deciding to do the first step; her speechballoon is directly interrupted by Wasabi (off camera).

> Pepper: Master Wasabi, my name is ...
> Wasabi: I know who you are.
>         My question is why there is a Chaosah witch in my presence?

---

Medium shot side view. Pepper blush a bit, shy and nervous with the way she was interrupted after her effort to break the ice. Wasabi's eyes meet Pepper's, but her face still points to her work.

> Pepper: I... I wanted to ask you for a job.
> Wasabi: A job? What use would I have for a Chaosah witch?

High angle shot on the desk, the hand of Wasabi continue to move papers from a stack to another. We can see various titles on the reports: "Chaosah Witches reports", "Zombiah new students", "Certifications for new Magmah potions". The actual text on the pages of the report unreadable.

> Wasabi: The rules of the treaty are very clear on what Chaosah Witches may do.
>         What do you possibly think you can do for me?

a. Focus on Pepper's nervous face, again cut by Wasabi speechbubble.

> Pepper: I, uh, I'm really good at a lot of things. 
>         I've ...
> Wasabi: Yes, yes, I know. 
>         I've seen what you can do.

b. Wasabi, close-up. Her face is more relaxed and curious.

> Wasabi: You're not like the other Chaosah witches, are you?

c. Wasabi, same shot, but her eyes go back to her paperwork while she takes a paper in hand.

> Wasabi: Tell you what. 
>         I need someone to guard the entry of the demon's gate of the north.

---

Wide shot on the hand of Wasabi giving a piece of paper in the direction of Pepper.

> Wasabi: Do you think you can handle watching a closed gate?

Medium shot on Pepper, surprised, she takes the paper. 

> Pepper: Yes ma'am. I'll do my best.

Full shot from Pepper to the gate behind her; Shichimi enters back the door with a big stack of paper, she does quickly. Pepper turns her back surprised.

> Shichimi: The Hippiah reports, master! 

Large shot on the desk while Wasabi get back to read her work.

> Wasabi: Good, good. Leave them here.
>         Could you conduct Pepper to the briefing room and explain to her the formalities of her new job?

Small reaction shot on Shichimi a bit confused and looking on her left in direction of Pepper.

> Shichimi: ?...

----

A wide shot of Pepper holding the paper, smiling at Shichimi and Shichimi understanding and smiling back.

Medium shot on Shichimi already at the door.

> Shichimi: Sure, Ma'am. 
>           Comon Pepper, follow me, I'll guide you!

Large shot of Pepper in the desk, just before leaving she turns her back to Wasabi but stop walking.

Close up on Pepper when she hold the door, she smile and can't refrain herself to express her gratitude, her way before leaving.

> Pepper: You're not nearly as mean as Thyme and Cayenne say you are

Close up on Wasabi, giving the side-eye into Pepper's direction.

---

Large shot on a room in the temple of Ah with many requirements; Shichimi is already inside and Pepper cross the door.

> Shichimi: So here is the map to go to the gate, 
>           your official gatekeeper uniform,
>           your authorization and an advance on your first salary.

Shot on Pepper with a large leather 'purse' with gold in it; she has big eyes on it.

> Pepper: ?!

Shot on Shichimi worrying about Pepper freezing.

> Shichimi: Pepper, something is wrong?

Pepper have eyes with tears in it and bite her lips.

Final panel: Pepper is at the table of a restaurant at night (in land of Ah, a plein air one, with beautiful paper lantern) with many things on the table, many beverages, cakes, candies, etc. She has a pure joy moment. Carrot is also having a good time. Shichimi also has a good time.

> Pepper: Wooohooo!! First salary!

---
