# Transcript of Pepper&Carrot Episode 13 [en]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Episode 13: The Pyjama Party

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|4|False|It's just so nice here!
Pepper|5|False|Thanks for inviting us Coriander!
Pepper|3|False|... ever !
Pepper|1|True|Best ...
Pepper|2|True|... holidays ...

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Coriander|1|True|Thanks !
Coriander|2|False|I hope you're not too intimidated by my staff.
Pepper|11|False|No, don't worry, they're very welcoming, we feel right at home.
Shichimi|13|False|... so refined and cosy at the same time!
Pepper|14|False|That's true!
Shichimi|12|True|I'm really impressed with the interior decoration...
Monster|9|False|WoooOoh ! ! !|nowhitespace
Monster|10|False|WoOoh ! ! !|nowhitespace
Monster|8|False|WoooOoh ! ! !|nowhitespace
Sound|7|False|shwwwwwwiiing !|nowhitespace
Sound|6|False|Klaf !|nowhitespace
Sound|5|False|Klukf !|nowhitespace
Sound|3|False|Dzzziii ! ! !|nowhitespace
Sound|4|False|Shkak ! ! !|nowhitespace

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Coriander|2|True|... sometimes I find it all so ...
Shichimi|5|True|I'd love to live in this kind of comfort,
Sound|7|False|Shiingz ! ! !|nowhitespace
Sound|4|False|bam !|nowhitespace
Shichimi|8|False|... "A-true-witch-of-Ah-must-not-live-in-that-fashion" .
Pepper|9|True|Ha ha !...
Pepper|10|True|The weight of tradition...
Pepper|11|False|It's like something my godmothers would say!
Shichimi|12|False|Really ?
Coriander|1|True|It's nice to remind me ...
Coriander|3|False|... "banal" ?...
Shichimi|6|False|but ...

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Coriander|2|False|Come on girls, let's concentrate, we're almost at the end!
Pepper & Shichimi|1|False|Tee-hee-hee!
Pepper|3|True|Oh, it's alright ... !
Pepper|5|False|... and we haven't even started to...
Sound|6|False|PHRoooOwwoww ! ! !|nowhitespace
Sound|9|False|Shhshh|nowhitespace
Shichimi|8|False|?!!
Coriander|7|False|PEPPER ! !!|nowhitespace
Pepper|4|True|You have to admit, this quest is super easy...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|6|False|C R A C K ! !|nowhitespace
Shichimi|8|False|Oh NO!
Coriander|9|False|Not again !
Pepper|11|False|Drop that ! Right now !
Coriander|1|False|...Oh no! It's too late! she... she's...
Shichimi|2|False|N oo oo oo !!!|nowhitespace
Monster|3|False|MUAH HAHA HAHA !!!|nowhitespace
Shichimi|4|False|G RR ! ! !|nowhitespace
Coriander|5|False|YOU WILL PAY FOR THAT ! ...
Sound|7|False|KLING ! !|nowhitespace
Sound|13|False|P LO NK ! !|nowhitespace
Sound|12|False|P AF ! !|nowhitespace
Pepper|10|True|CARROT!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|10|False|11/2015 - Art & Scenario: David Revoy Translation: Alex Gryson
Sound|1|False|Pam|nowhitespace
Pepper|4|True|I know you're only trying to defend me...
Pepper|3|True|Come now, Don't pout!
Pepper|5|False|... but there's no need, we're only playing!
Carrot|8|False|Grrrr
Writing|6|False|Citadels & Phoenixes
Writing|2|False|Princess Coriander
Narrator|9|False|- FIN -
Writing|7|False|Citadels & Phoenixes

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Pepper&Carrot is entirely free(libre), open-source and sponsored thanks to the patronage of its readers. For this episode, thank you to the 602 Patrons:
Credits|2|True|You too can become a patron of Pepper&Carrot for the next episode at
Credits|3|False|https://www.patreon.com/davidrevoy
Credits|4|False|License : Creative Commons Attribution 4.0 Source : available at www.peppercarrot.com Software : this episode was 100% drawn with libre software Krita 2.9.9, Inkscape 0.91 on Linux Mint 17
