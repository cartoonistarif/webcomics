# Transcript of Pepper&Carrot Episode 33 [da]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 33: Krigs-trylleformularen

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fjende|1|False|trUUUUUUUUUUUt !
Hær|2|False|Roaaaar!!
Hær|5|False|Grroooaarr!!
Hær|4|False|Grrrr!
Hær|3|False|Juurrr!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Kongen|1|True|Okay, unge heks.
Kongen|2|False|Hvis du har en trylle-formular, der kan hjælpe os, er det nu eller aldrig!
Pepper|3|True|Det er modtaget!
Pepper|4|False|Vær klar til at se...
Lyd|5|False|Dzziii! !|nowhitespace
Pepper|6|False|...MIT MESTERVÆRK!!
Lyd|7|False|Dzziiii! !|nowhitespace
Pepper|8|False|Realitas Hackeris Pepperus !
Lyd|9|False|Dzziuuu! !|nowhitespace

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|1|False|Fiizz!
Lyd|2|False|Dzii!
Lyd|3|False|Shiii!
Lyd|4|False|Ffhii !
Lyd|8|False|Dziing!
Lyd|7|False|Fiizz!
Lyd|6|False|Shiii!
Lyd|5|False|Ffhii!
Kongen|9|True|Kan det være en formular, der giver mere kraft til vores sværd...
Kongen|10|False|...og gør vores fjenders våben svagere?
Lyd|11|False|Dzii...
Pepper|12|True|He, he.
Pepper|13|True|Det for De at se!
Pepper|14|False|Det jeg kan sige er, at De ikke kommer til at miste nogen soldater i dag.
Pepper|15|False|Men I skal alligevel kæmpe, så godt I kan!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Kongen|1|False|Det er dét, vi er trænet til!
Kongen|2|False|PÅ MIN BEFALING!
Hær|3|False|Jaaa!
Hær|4|False|Yaaaa!
Hær|5|False|Jaaa!
Hær|6|False|Jaaa !
Hær|7|False|Yaaaa!
Hær|8|False|Jaaah!
Hær|9|False|Yihaaa!
Hær|10|False|Yahii!
Hær|11|False|Jaa!
Kongen|12|False|TIL ANGREEEEEEB!!!
Hær|13|False|Yaa!
Hær|14|False|Jaaa!
Hær|15|False|Yihaaa!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Kongen|1|False|Jihaaaa!!
Fjende|2|False|YUuurr!!!
Lyd|3|False|Tchiiiing! !|nowhitespace
Lyd|4|False|Swooosh! !|nowhitespace
Skrift|5|False|12
Fjende|6|False|?!!
Skrift|7|False|8
Kongen|8|False|?!!
Skrift|9|False|64
Skrift|10|False|32
Skrift|11|False|72
Skrift|12|False|0
Skrift|13|False|64
Skrift|14|False|0
Skrift|15|False|56
Hær|20|False|Jrrr!
Hær|17|False|Yurr!
Hær|19|False|Grrr!
Hær|21|False|Jaaa!
Hær|18|False|Jaaa!
Hær|16|False|Yihaa!
Lyd|27|False|Sshing
Lyd|25|False|Fffshh
Lyd|22|False|wiizz
Lyd|23|False|Swoosh
Lyd|24|False|Shklong
Lyd|26|False|Shkilng

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Kongen|1|False|?!
Skrift|2|False|3
Skrift|3|False|24
Skrift|4|False|38
Skrift|5|False|6
Skrift|6|False|12
Skrift|7|False|0
Skrift|8|False|5
Skrift|9|False|0
Skrift|10|False|37
Skrift|11|False|21
Skrift|12|False|62
Skrift|13|False|27
Skrift|14|False|4
Kongen|15|False|! !|nowhitespace
Kongen|16|False|HEEEEEEKS! HVAD HAR DU GJORT?!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Tadaaa !
Pepper|2|True|"Dette"
Pepper|3|False|...er mit mesterværk!
Pepper|4|False|Det er en kompleks trylleformular, som ændrer virkeligheden og viser hvor mange livspoint jeres modstander har tilbage.
Skrift|5|False|33
Pepper|6|True|Når I ikke har flere livspoint, går I væk fra slagmarken, indtil kampen er slut.
Pepper|7|True|Den første hær, der besejrer alle modstanderens soldater, vinder!
Pepper|8|False|Nemt nok.
Skrift|9|False|0
Hær|10|False|?
Pepper|11|True|Er det ikke genialt?
Pepper|12|True|Ikke flere døde!
Pepper|13|True|Ikke flere skadede soldater!
Pepper|14|False|Det vil revolutionere måden at føre krig på!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Nu hvor I kender reglerne, så lad mig nulstille pointene!
Pepper|2|False|Så er det mere retfærdigt.
Skrift|3|False|64
Skrift|4|False|45
Skrift|5|False|6
Skrift|6|False|2
Skrift|7|False|0
Skrift|8|False|0
Skrift|9|False|0
Skrift|10|False|0
Skrift|11|False|9
Skrift|12|False|5
Skrift|13|False|0
Skrift|14|False|0
Skrift|15|False|0
Lyd|16|False|Sshing!
Lyd|17|False|Suuu!
Lyd|19|False|tchak!
Lyd|18|False|Pok!
Pepper|20|True|Løb hurtigere, Carrot!
Pepper|21|False|Trylleformularen virker ikke meget længere!
Fortæller|22|False|- Slut -

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|1|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|2|False|You can also translate this page if you want.
<hidden>|3|False|Beta readers help with the story, proofreaders give feedback about the text.
Credits|4|False|Den 29. juni 2020 Tegning og manuskript: David Revoy. Genlæsning i beta-versionen: Arlo James Barnes, Craig Maloney, GunChleoc, Hyperbolic Pain, Karl Ove Hufthammer, Martin Disch, Nicolas Artance, Quetzal2, Valvin. Dansk Version Oversættelse: Emmiline Alapetite Rettelser: Rikke & Alexandre Alapetite Baseret på Hereva-universet Skabt af: David Revoy. Vedligeholdt af: Craig Maloney. Medforfattere: Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Rettelser: Willem Sonke, Moini, Hali, CGand, Alex Gryson . Værktøj: Krita 4.3, Inkscape 1.0 on Kubuntu 19.10. Licens: Creative Commons Attribution 4.0. www.peppercarrot.com
Pepper|5|True|Vidste I det?
Pepper|6|True|Pepper & Carrot er fri, open-source og sponsoreret af sine læsere.
Pepper|7|False|Denne episode blev støttet af 1190 tilhængere!
Pepper|8|True|Du kan også blive tilhænger og få dit navn skrevet her!
Pepper|9|True|Vi er på Patreon, Tipeee, PayPal, Liberapay … og andre!
Pepper|10|True|Gå på www.peppercarrot.com og få mere information!
Pepper|11|False|Tak!
