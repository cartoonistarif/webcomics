# Transcript of Pepper&Carrot Episode 33 [pl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Odcinek 33: Zaklęcie wojny

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Wróg|1|False|WUUŁUUUUUUOOOOO !
Armia|2|False|Raaaaar!
Armia|5|False|Grraaaar!
Armia|4|False|Grrrr!
Armia|3|False|Jarrr!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Król|1|True|Już czas, młoda wiedźmo.
Król|2|False|Jeśli masz w zana-drzu jakieś zaklęcie, które może pomóc, użyj go.
Pepper|3|True|Tak jest!
Pepper|4|False|Przygotuj-cie się na
Sound|5|False|Dzziii! ! !|nowhitespace
Pepper|6|False|MOJE ARCYDZIEŁO!!!
Sound|7|False|Dzziiii! ! !|nowhitespace
Pepper|8|False|Realitas Hackeris Pepperus!
Sound|9|False|Dzziuuu! ! !|nowhitespace

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|False|Fiizz!
Sound|2|False|Dzii!
Sound|3|False|Szzii!
Sound|4|False|Ffhii!
Sound|8|False|Dziing!
Sound|7|False|Fiizz!
Sound|6|False|Szzii!
Sound|5|False|Ffhii!
Król|9|True|Czyli to zaklęcie wzmacnia nasze miecze
Król|10|False|i osłabia broń wrogów?
Sound|11|False|Dzii...
Pepper|12|True|Hehe,
Pepper|13|True|przekonasz się.
Pepper|14|False|Powiem tylko, że dzisiaj żaden rycerz nie padnie w boju.
Pepper|15|False|Lecz nadal będziecie mu-sieli walczyć i dać z sie-bie wszystko.

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Król|1|False|Do tego byliśmy szkoleni.
Król|2|False|NA MÓJ ROZKAZ!
Armia|3|False|Haaa!
Armia|4|False|Taaaak!
Armia|5|False|Taa!
Armia|6|False|Jaaaa!
Armia|7|False|Taaaaaaa!
Armia|8|False|Aaaaaa!
Armia|9|False|Haaaaaa!
Armia|10|False|Aaaa!
Armia|11|False|Taaaak!
Król|12|False|NAPRZÓD!!!
Armia|13|False|Yeaahh!
Armia|14|False|Yahhh!
Armia|15|False|Yaeeh!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Król|1|False|Aaaaaaa!!!
Wróg|2|False|Jeeeeeeech!
Sound|3|False|Szzziuuum!!!
Sound|4|False|Ciaaaach!
Writing|5|False|12
Wróg|6|False|?!
Writing|7|False|8
Król|8|False|?!
Writing|9|False|64
Writing|10|False|32
Writing|11|False|72
Writing|12|False|0
Writing|13|False|64
Writing|14|False|0
Writing|15|False|56
Armia|20|False|Urrr!
Armia|17|False|Jurr!
Armia|19|False|Grrr!
Armia|21|False|Jaaaa!
Armia|18|False|Taaaaak!
Armia|16|False|Jaaaaa!
Sound|27|False|Szzing
Sound|25|False|Fffchh
Sound|22|False|wiizz
Sound|23|False|Siuuu
Sound|24|False|Czklong
Sound|26|False|Czkilng

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Król|1|False|?!
Writing|2|False|3
Writing|3|False|24
Writing|4|False|38
Writing|5|False|6
Writing|6|False|12
Writing|7|False|0
Writing|8|False|5
Writing|9|False|0
Writing|10|False|37
Writing|11|False|21
Writing|12|False|62
Writing|13|False|27
Writing|14|False|4
Król|15|False|! !|nowhitespace
Król|16|False|WIEDŹMO!!! COŚ TY NAROBIŁA?!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Tadaaa!
Pepper|2|True|„To”
Pepper|3|False|moje arcydzieło!
Pepper|4|False|Skomplikowane zaklęcie zmieniające rzeczywistość i ukazujące punkty życia przeciwników.
Writing|5|False|33
Pepper|6|True|Gdy spadną do zera, opuszczasz pole bitwy, aż do jej końca.
Pepper|7|True|Armia, która pierwsza pozbędzie się przeciwni-ków, wygrywa.
Pepper|8|False|Proste.
Writing|9|False|0
Armia|10|False|?
Pepper|11|True|Czyż to nie wspaniałe?
Pepper|12|True|Nikt nie umrze!
Pepper|13|True|Nie będzie rannych!
Pepper|14|False|Wojna już nigdy nie będzie taka straszna!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Skoro już wyjaśniłam zasady, pozwól, że zresetuję wyniki, byśmy mogli zacząć od początku.
Pepper|2|False|Tak będzie fair.
Writing|3|False|64
Writing|4|False|45
Writing|5|False|6
Writing|6|False|2
Writing|7|False|0
Writing|8|False|0
Writing|9|False|0
Writing|10|False|0
Writing|11|False|9
Writing|12|False|5
Writing|13|False|0
Writing|14|False|0
Writing|15|False|0
Sound|16|False|Siuuum!
Sound|17|False|Wzuuu!
Sound|19|False|Trach!
Sound|18|False|Pac!
Pepper|20|True|Zwiewaj, Carrot!
Pepper|21|False|Zaklęcie niedługo się wyczerpie!
Narrator|22|False|- KONIEC -

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|1|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|2|False|You can also translate this page if you want.
<hidden>|3|False|Beta readers help with the story, proofreaders give feedback about the text.
Credits|4|False|29 czerwca, 2020 Rysunki i scenariusz: David Revoy. Poprawki skryptu: Arlo James Barnes, Craig Maloney, GunChleoc, Hyperbolic Pain, Karl Ove Hufthammer, Martin Disch, Nicolas Artance, Quetzal2, Valvin. Wersja polska Tłumaczenie: Sölve Svartskogen. Korekta i kontrola jakości: Besamir. Oparto na uniwersum Herevy Autor: David Revoy. Pomocnik: Craig Maloney. Pisarze: Craig Maloney, Nartance, Scribblemaniac, Valvin. Korekta: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Software: Krita 4.3, Inkscape 1.0 na Kubuntu 19.10. Licencja: Creative Commons Uznanie autorstwa 4.0. www.peppercarrot.com
Pepper|5|True|Wiedziałeś, że
Pepper|6|True|Pepper&Carrot jest całkowicie darmowy, open-source'owy oraz wspierany przez naszych czytelników?
Pepper|7|False|Za ten odcinek dziękujemy 1190 patronom!
Pepper|8|True|Ty również możesz zostać patronem Pepper&Carrot i znaleźć się na tej liście.
Pepper|9|True|Jesteśmy na Patreonie, Tipeee, PayPal, Liberapay i wielu innych!
Pepper|10|True|Wejdź na www.peppercarrot.com, by dowiedzieć się więcej!
Pepper|11|False|Dziękujemy!
