# Transcript of Pepper&Carrot Episode 29 [fr]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titre|1|False|Épisode 29 : Le Destructeur de Mondes

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Monstre|2|True|…Enfin !
Monstre|3|False|Un déchirement interdimensionnel !
Monstre|4|False|Sans doute causé par un alignement d'astre ou un grand événement !
Monstre|5|True|Grandis… Grandis, petite faille !
Monstre|6|False|Et dévoile-moi ce nouveau monde à ASSERVIR ET DOMINER !
Monstre|7|True|Oh oh…
Monstre|8|False|Voilà quelque chose de bien intéressant.
Monstre|9|False|On peut même dire que c'est mon jour de chance…
Narrateur|1|False|Au même moment, dans une autre dimension…

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Monstre|1|False|Une dimension peuplée de créatures intelligentes !
Monstre|2|False|Grandis, petite faille !
Monstre|3|False|Gŗandis… Mouhaha hahaha !
Pepper|4|True|Pfiou !
Pepper|5|False|Mais quelle soirée mes amies !
Coriandre|6|False|Pepper, j'étais justement en train de réaliser avec Safran qu'on avait aucune idée de comment fonctionnait ta magie, Chaosah.
Safran|7|False|C'est bien mystérieux, tu pourrais nous en dire un peu plus ?

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Haha, ça va être un peu compliqué !
Pepper|2|True|Mais bon, on peut dire que c'est essentiellement basé sur la compréhension des lois derrière tous les systèmes chaotiques…
Pepper|3|False|…des plus petits aux plus grands.
Coriandre|4|False|C'est certain que dit comme ça, c'est tout de suite plus clair…
Pepper|7|False|Rhôô ! Attendez, je vais vous faire un exemple tout simple.
Pepper|8|False|Laissez-moi juste une minute avec un peu de bon sens de Chaosah…
Pepper|9|False|Trouvé !
Son|5|True|GRAT
Son|6|False|GRAT
Pepper|10|True|Regardez ce pic à boulettes qui était coincé entre deux pavés.
Pepper|11|False|Le fait de l'avoir trouvé et ramassé évitera certainement à quelqu'un de se le prendre dans les pieds.
Pepper|12|True|Un petit changement positif dans le grand système chaotique de l'existence, qui peut avoir d'énormes conséquences.
Pepper|13|False|C'est ça Chaosah !

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|False|Oh beurk !
Coriandre|2|False|Pouah ! Ce truc était dans la bouche de quelqu'un !
Safran|3|False|Haha ! Impressionnant comme toujours, Pepper !
Coriandre|4|False|Bon, merci Pepper pour cette… « explication ».
Coriandre|5|True|Il est temps d'aller dormir vous croyez pas ?
Coriandre|6|False|Et de se laver les mains pour certaines.
Pepper|7|False|Eh ! Mais attendez-moi !
Son|8|False|Poc !
Son|9|True|Poc !
Son|10|False|Poc !
Son|11|False|P AF !|nowhitespace
Carrot|12|False|?

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|1|False|Ssss !
Son|2|False|Poc !
Son|3|False|Toc !
Son|4|False|C LING !|nowhitespace
Son|5|False|Fr rfrf…|nowhitespace

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|We have a patch for the artwork in case your text is too short or is not easily split in two words. Ask David or Midgard for more information. Look at the Korean version for an example.
<hidden>|0|False|NOTE FOR TRANSLATORS
Monstre|7|False|À L'ATTAQUE !!!
Monstre|6|True|Mouhaha HA HA ! Enfin !
Monstre|9|False|!?
Écriture|1|True|ENTREPÔT
Écriture|2|False|FEUX D'ARTIFICE
Son|3|False|Fr rOuf ! !|nowhitespace
Son|4|False|Fr r ! !|nowhitespace
Son|5|False|BOUM !
Son|8|False|Zwiff ! !|nowhitespace

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|3|False|B oUM !|nowhitespace
Son|2|False|B OU M!|nowhitespace
Son|1|False|P A F !|nowhitespace
Son|4|False|Zwiff ! !|nowhitespace
Son|5|False|PoU F !|nowhitespace
Pepper|6|True|T'inquiète pas, Carrot.
Pepper|7|False|Ça doit juste être des gens qui font encore la fête…
Pepper|8|False|Allez, au dodo !

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Monstre|5|False|...
Cumin|1|False|Vraiment ? …Elle aurait réussi une réaction en chaîne de si grande échelle sans même s'en rendre compte ?
Cayenne|2|False|Aucun doute là-dessus.
Thym|3|False|Mesdames, je crois que notre Pepper est enfin prête !
Son|6|False|Plop !
Son|4|False|Pch hh ! !|nowhitespace
Narrateur|7|False|- TRILOGIE DU SACRE DE CORIANDRE, FIN -

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|5|True|Vous pouvez aussi devenir mécène de Pepper&Carrot et avoir votre nom inscrit ici !
Pepper|3|True|Pepper&Carrot est entièrement libre, gratuit, open-source et sponsorisé grâce au mécénat de ses lecteurs.
Pepper|4|False|Cet épisode a reçu le soutien de 960 mécènes !
Pepper|7|True|Allez sur www.peppercarrot.com pour plus d'informations !
Pepper|6|True|Nous sommes sur Patreon, Tipeee, PayPal, Liberapay …et d'autres !
Pepper|8|False|Merci !
Pepper|2|True|Le saviez-vous ?
Crédits|1|False|le 25 Avril 2019 Art & scénario : David Revoy. Lecteurs de la version bêta : CalimeroTeknik, Craig Maloney, Martin Disch, Midgard, Nicolas Artance, Valvin. Version française Corrections : CalimeroTeknik. Basé sur l'univers d'Hereva Créateur : David Revoy. Mainteneur principal : Craig Maloney. Écrivains : Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Correcteurs : Willem Sonke, Moini, Hali, CGand, Alex Gryson . Logiciels : Krita 4.1.5~appimage, Inkscape 0.92.3 sur Kubuntu 18.04.1. Licence : Creative Commons Attribution 4.0. www.peppercarrot.com
<hidden>|0|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|0|False|You can also translate this page if you want.
<hidden>|0|False|Beta readers help with the story, proofreaders give feedback about the text.
