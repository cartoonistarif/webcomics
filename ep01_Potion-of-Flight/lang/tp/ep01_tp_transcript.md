# Transcript of Pepper&Carrot Episode 01 [tp]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
nimi|1|False|wan nanpa wan: telo pi tawa waso

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|False|sin la... pini!
jan Pepa|4|False|...a. ken la ona o mute
kalama|2|True|SA
kalama|3|False|SA
kalama|5|True|PO
kalama|6|False|PO

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|False|a... pona a!
jan Pepa|2|False|ike! o pali ala
kalama|3|False|P UN S A|nowhitespace

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|False|sina pilin pona anu seme ?!
mama|2|False|WWW.PEPPERCARROT.COM 05/2014
