# Transcript of Pepper&Carrot Episode 01 [de]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 1: Der Flugtrank

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|...und noch der letzte Schliff
Pepper|4|False|...hmm, war wohl nicht stark genug
Geräusch|2|True|SHH
Geräusch|3|False|SHH
Geräusch|5|True|PLOP
Geräusch|6|False|PLOP

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|ha... perfekt
Pepper|2|False|NEIN! Pfoten weg!
Geräusch|3|False|S PL A S H|nowhitespace

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Zufrieden ?!
Impressum|2|False|WWW.PEPPERCARROT.COM 05/2014
