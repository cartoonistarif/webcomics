# Transcript of Pepper&Carrot Episode 04 [gd]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tiotal|1|False|Eapasod 4: An sàr-bheachd

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Neach-aithris|1|False|An oidhche ro dhùbhlan nan deoch 2:00 m
Peabar|2|True|Mu dheireadh thall ... Leis an deoch seo, dearbhaidh mi mo chomas thar chàich agus fiù Cròch !
Peabar|3|False|Feumaidh mi a cur fo dheuchainn ro làimh ge-tà ...
Peabar|4|False|A CHURRAAA~ ~AAAAAIN !!!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sgrìobhte|14|False|Biadh CAIT
Peabar|1|False|A Churrain ?
Peabar|2|False|Annasach! Chan eil e fiù ’s fon leabaidh !
Peabar|4|False|A Churrain ?
Peabar|5|False|A Churrain ?!
Peabar|6|False|A Churrain ?!!!
Peabar|3|False|A Churrain !
Peabar|7|False|Cha shaoilinn gum bithinn feumach air a’ char seo ...
Peabar|8|False|A Churrain a sheòid !
Fuaim|9|True|Crrr
Fuaim|10|True|Crrr
Fuaim|11|True|Crrr
Fuaim|12|True|Crrr
Fuaim|13|False|Crrr
Fuaim|15|True|Sssioft
Fuaim|16|True|Sssioft
Fuaim|17|False|Sssssioft
Fuaim|19|False|Sssioft Sssioft Sssssioft
Fuaim|18|False|Crrr Crrr Crrr Crrr Crrr
Fuaim|21|False|Crrr Crrr Crrr Crrr Crrr
Fuaim|22|False|Sssioft Sssioft Sssssioft
Curran|23|False|Rùdail
Fuaim|24|False|Fruuuiss !
Curran|20|False|O sole miamh !

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|1|True|A Churrain m’ eudail ! A stidein mhilis agus thàinig thu thugam air do thoil fhèin
Peabar|2|False|’S an dearbh uair a bha mi feumach air a’ chuidiche as fheàrr leam
Peabar|3|True|Seo dhut an t-annas-làimhe deireannach agam
Peabar|4|False|deoch an t-sàr-bheachd
Peabar|5|False|Nach gabh tu balgam dhi
Peabar|6|False|Ma dh’obraicheas i, thig sàr-bheachd a-steach ort an ceartuair

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fuaim|1|False|Poc
Peabar|3|False|A bhalaich ort ... a Churrain ! Tha seo iongantach ... Seo litir cheart na h-aibidil ...
Peabar|5|False|Tha ... tha thu a’ sgrìobhadh !
Peabar|6|False|E ?
Peabar|7|False|Eadha ?
Peabar|9|False|Eirmse ?
Peabar|10|False|Eòlas ?
Fuaim|4|False|Sgrrrriiiiiii
Fuaim|2|False|Sgriiiiiobh
Peabar|8|False|Eun ?

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|1|False|Dè eile a b’ urrainn dha bhith?
Peabar|2|False|Eumhais ?
Peabar|3|False|Eumu ?
Peabar|4|False|Tud, a Churrain ! Tha na faclan a' ruith orm agus cha dèan mi bun no bàrr dheth !
Peabar|5|False|... cha dèan mi stem dheth idir ...
Peabar|6|True|Grrrrrrr !!!
Peabar|7|False|chan obraich dad sam bith dhomh-sa !
Peabar|8|True|Coma leat ...
Peabar|9|False|'s tha ùine agam fhathast airson deoch eile a thionnsgnadh
Fuaim|10|False|CN...A...GGGGG !
Peabar|11|False|Cadal sèimh a Churrain
Neach-aithris|12|False|Ri leantainn ...
Urram|13|False|David Revoy 11/2014

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Urram|3|False|Алексей - 獨孤欣 - Adrian Lord - Alan Hardman - Albert Westra - Alejandro Flores Prieto - Alexander Sopicki - Alex Lusco Alex Silver - Alex Vandiver - Alfredo - Alice Queenofcats - Ali Poulton (Aunty Pol) - Alison Harding - Allan Zieser Andreas Rieger - Andrew - Andrew Godfrey - Andrey Alekseenko - Angela K - Anna Orlova - anonymous - Antan Karmola Arco - Ardash Crowfoot - Arne Brix - Aslak Kjølås-Sæverud - Axel Bordelon - Axel Philipsenburg - barbix - Ben Evans Bernd - Boonsak Watanavisit - Boudewijn Rempt - Brian Behnke - carlos levischi - Charlotte Lacombe-bar - Chris Radcliff Chris Sakkas - Christophe Carré - Clara Dexter - Colby Driedger - Conway Scott Smith - Cyrille Largillier - Cyril Paciullo Daniel - david - Damien de Lemeny - Dmitry - Donald Hayward - Dubema Ulo - Eitan Goldshtrom - Enrico Billich-epsilon--Eric Schulz - Garret Patterson - Guillaume - Gustav Strömbom - Guy Davis - Happy Mimic - Helmar Suschka Ilyas Akhmedov - Inga Huang - Irene C. - Ivan Korotkov - Jared Tritsch - JDB - Jean-Baptiste Hebbrecht - Jessey Wright John - Jónatan Nilsson - Jon Brake - Joseph Bowman - Jurgo van den Elzen - Justin Diehl - Kai-Ting (Danil) Ko - Kasper Hansen Kathryn Wuerstl - Ken Mingyuan Xia - Liselle - Lorentz Grip - MacCoy - Magnus Kronnäs - Mandy - marcus - Martin Owens Masked Admirer - Mathias Stærk - mattscribbles - Maurice-Marie Stromer - mefflin ross bullis-bates - Michael Gill Michelle Pereira Garcia - Mike Mosher - Morten Hellesø Johansen - Muzyka Dmytro - Nazhif - Nicholas DeLateur Nick Allott - Nicki Aya - Nicolae Berbece - Nicole Heersema - Noah Summers - Noble Hays - Nora Czaykowski Nyx - Olivier Amrein - Olivier Brun - Omar Willey - Oscar Moreno - Öykü Su Gürler - Ozone S. - Paul - Paul Pavel Semenov - Peter - Peter Moonen - Petr Vlašic - Pierre Geier - Pierre Vuillemin - Pranab Shenoy - Rajul Gupta Ret Samys - rictic - RJ van der Weide - Roman - Rumiko Hoshino - Rustin Simons - Sami T - Samuel Mitson - Scott RussSean Adams - Shadefalcon - Shane Lopez - Shawn Meyer - Sigmundur Þórir Jónsson - Simon Forster - Simon IsenbergSonny W. - Stanislav Vodetskyi - Stephen Bates - Steven Bennett - Stuart Dickson - surt - Tadamichi Ohkubo - tar8156TheFaico - Thomas Schwery - Tracey Reuben - Travis Humble - Vespertinus - Victoria - Vlad Tomash - Westen CurryXavier Claude - Yalyn Vinkindo agus taing shònraichte do dh'Amireeti a chuidich le ceartachadh na Beurla !
Urram|1|False|Tha Pepper&Carrot saor is le tùs fosgailte air fad agus 'ga sponsaireadh le taic phàtranan. Thug 156 pàtran taic dhan eapasod seo :
Urram|6|False|https://www.patreon.com/davidrevoy
Urram|5|False|Nach cuir thu taic dhan phroiseact? Bheir tabhartas dhe dholar air gach dealbh-èibhinn taic mhor dha Pepper&Carrot !
Urram|9|False|Innealan : Chaidh 100% dhen eapasod seo a dhèanamh le innealan saora aig a bheil bun-tùs fosgailte Krita, G'MIC, Blender, GIMP air Ubuntu Gnome (GNU/Linux)
Urram|8|False|Tùs fosgailte : faidhlichean bun-tùis le breathan air càileachd àrd a ghabhas clò-bhualadh, le cruthan-clò ris a ghabhas luchdadh a-nuas, a reic, atharrachadh, eadar-theangachadh is msaa...
Urram|7|False|Ceadachas : Creative Commons Attribution dha "David Revoy" 's faodaidh tu obair eile bonntachadh air, atharrachadh, ath-phostadh, a reic is msaa...
Curran|4|False|Mòran taing !
