# Transcript of Pepper&Carrot Episode 32 [id]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Episode 32: Medan Tempur

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
King|1|True|Perwira!
King|2|False|Apa kamu sudah merekrut penyihir seperti yang aku minta?
Officer|3|True|Sudah, tuan!
Officer|4|False|Dia sedang berdiri di sebelah tuan.
King|5|False|...?
Pepper|6|True|Hai!
Pepper|7|False|Namaku Pepp...
King|8|False|?!!
King|9|True|BODOH!!!
King|10|True|Kenapa kamu merekrut anak kecil?!
King|11|False|Aku perlu penyihir perang sungguhan!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Maaf!
Pepper|2|True|Aku ini Penyihir Chaosah sungguhan.
Pepper|3|False|Aku bahkan punya ijazah yang isinya...
Writing|4|False|Ijazah
Writing|5|False|Kelulusan
Writing|6|False|Chaosah
Writing|7|False|Cayenne
Writing|8|False|Cumin
Writing|9|False|T h yme|nowhitespace
Writing|10|False|~ Pepper ~
King|11|False|DIAM!
King|12|True|Aku tidak perlu anak kecil di pasukanku.
King|13|False|Sana pulang dan main dengan bonekamu.
Sound|14|False|Plak!
Army|15|True|HAHA HA HA!
Army|16|True|HAHA HA HA!
Army|17|True|HAHA HA HA!
Army|18|False|HA HA HAHA!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Aku tidak percaya ini!
Pepper|2|False|Aku sudah belajar bertahun-tahun, tapi tidak ada yang menganggap aku serius karena...
Pepper|3|False|...aku tidak terlihat cukup berpengalaman!
Sound|4|False|PUFF ! !|nowhitespace
Pepper|5|False|CARROT !|nowhitespace
Sound|6|False|PAF ! !|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Sungguh, Carrot?
Pepper|2|True|Semoga makanan itu sebanding ya.
Pepper|3|False|Kamu terlihat mengerikan...
Pepper|4|False|...Kamu terlihat...
Pepper|5|True|Penampilannya!
Pepper|6|False|Tentu saja!
Sound|7|False|Krek!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|HEI!
Pepper|2|False|Aku dengar kamu mencari PENYIHIR SUNGGUHAN ?!?

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
King|1|True|Setelah dipikir-pikir, panggil anak itu lagi.
King|2|False|Yang ini sepertinya akan terlalu mahal.
Writing|3|False|BERSAMBUNG...

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|5|True|Anda juga bisa menjadi sukarelawan Pepper&Carrot dan dapatkan namamu di sini!
Pepper|3|True|Pepper&Carrot sepenuhnya gratis (bebas), sumber terbuka dan disponsori oleh para pembacanya.
Pepper|4|False|Untuk episode ini, terima kasih untuk 1121 sukarelawan!
Pepper|7|True|Cek www.peppercarrot.com untuk informasi selanjutnya!
Pepper|6|True|Kami ada di Patreon, Tipeee, PayPal, Liberapay ...and banyak lagi!
Pepper|8|False|Terima kasih!
Pepper|2|True|Tahukah kamu?
Credits|1|False|31 Maret 2020 Pelukis & Skenario: David Revoy. Penguji Bacaan Cerita: Craig Maloney, Martin Disch, Arlo James Barnes, Nicolas Artance, Parnikkapore, Stephen Paul Weber, Valvin, Vejvej. Versi Bahasa Indonesia Penerjemah: Aldrian Obaja Muis . Berdasarkan alam semesta Hereva Pembuat: David Revoy. Penyunting Utama: Craig Maloney. Penulis: Craig Maloney, Nartance, Scribblemaniac, Valvin. Pemeriksa: Willem Sonke, Moini, Hali, CGand, Alex Gryson . Peranti Lunak: Krita 4.2.9-beta, Inkscape 0.92.3 on Kubuntu 19.10. Izin (Lisensi): Creative Commons Attribution 4.0. www.peppercarrot.com
<hidden>|9|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|10|False|You can also translate this page if you want.
<hidden>|11|False|Beta readers help with the story, proofreaders give feedback about the text.
