# Transcript of Pepper&Carrot Episode 32 [en]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Episode 32: The Battlefield

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
King|1|True|Officer!
King|2|False|Did you recruit a witch like I asked?
Officer|3|True|Yes, my lord!
Officer|4|False|She is standing beside you.
King|5|False|...?
Pepper|6|True|Hi!
Pepper|7|False|My name is Pepp...
King|8|False|?!!
King|9|True|FOOL!!!
King|10|True|Why have you recruited this child?!
King|11|False|I need a real battlefield witch!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Excuse me!
Pepper|2|True|I'm a real Chaosah Witch.
Pepper|3|False|I even have a diploma that says...
Writing|4|True|Degree
Writing|5|True|of
Writing|6|False|Chaosah
Writing|8|True|Cayenne
Writing|9|False|Cumin
Writing|7|True|T h yme|nowhitespace
Writing|10|False|~ for Pepper ~
King|11|False|SILENCE!
King|12|True|I have no use for children in this army.
King|13|False|Go home and play with your dolls.
Sound|14|False|Slap!
Army|15|True|HAHA HA HA!
Army|16|True|HAHA HA HA!
Army|17|True|HAHA HA HA!
Army|18|False|HA HA HAHA!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|I don't believe this!
Pepper|2|False|I've studied for years, but nobody will take me seriously because...
Pepper|3|False|...I don't look experienced enough!
Sound|4|False|POOF ! !|nowhitespace
Pepper|5|False|CARROT !|nowhitespace
Sound|6|False|PAF ! !|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Really, Carrot?
Pepper|2|True|I hope that meal was worth it.
Pepper|3|False|You look hideous...
Pepper|4|False|...You look...
Pepper|5|True|The appearance!
Pepper|6|False|Of course!
Sound|7|False|Crac !|nowhitespace

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|HEY!
Pepper|2|False|I heard you were looking for a REAL WITCH ?!?

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
King|1|True|On second thought, call that kid back.
King|2|False|This one probably costs too much.
Writing|3|False|TO BE CONTINUED…

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|5|True|You too can become a patron of Pepper&Carrot and get your name here!
Pepper|3|True|Pepper&Carrot is entirely free(libre), open-source and sponsored thanks to the patronage of its readers.
Pepper|4|False|For this episode, thanks go to 1121 patrons!
Pepper|7|True|Check www.peppercarrot.com for more info!
Pepper|6|True|We are on Patreon, Tipeee, PayPal, Liberapay ...and more!
Pepper|8|False|Thank you!
Pepper|2|True|Did you know?
Credits|1|False|March 31, 2020 Art & scenario: David Revoy. Beta readers: Craig Maloney, Martin Disch, Arlo James Barnes, Nicolas Artance, Parnikkapore, Stephen Paul Weber, Valvin, Vejvej. English version (original version) Proofreading: Craig Maloney, Martin Disch . Based on the universe of Hereva Creator: David Revoy. Lead maintainer: Craig Maloney. Writers: Craig Maloney, Nartance, Scribblemaniac, Valvin. Correctors: Willem Sonke, Moini, Hali, CGand, Alex Gryson . Software: Krita 4.2.9-beta, Inkscape 0.92.3 on Kubuntu 19.10. License: Creative Commons Attribution 4.0. www.peppercarrot.com
<hidden>|9|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|10|False|You can also translate this page if you want.
<hidden>|11|False|Beta readers help with the story, proofreaders give feedback about the text.
