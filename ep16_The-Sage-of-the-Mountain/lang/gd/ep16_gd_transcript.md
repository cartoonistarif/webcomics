# Transcript of Pepper&Carrot Episode 16 [gd]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tiotal|1|False|Eapasod 16: Seanair na beinne

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cròch|9|False|Cha bhruidhinn mise riutha, a Pheabar.
Sgrìobhte|2|True|Cròch
Sgrìobhte|3|False|Buidseachas
Sgrìobhte|5|False|BÙIDSEAR
Sgrìobhte|6|False|15
Sgrìobhte|4|False|13
Sgrìobhte|8|False|Rathad na Rèile
Sgrìobhte|7|False|GRUAGAIRE
Fuaim|10|True|Glug
Fuaim|11|False|Glug
Sgrìobhte|1|False|★ ★ ★ ★

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cròch|2|False|Gu pearsanta, cha dèan mi eadragainn fiù ’s mas e às leth caraid a th’ ann. Cha do dh’èirich ach am beud às a-riamh.
Cròch|1|True|Feumaidh tu fhèin an làimhseachadh.
Peabar|3|False|C-cuidich mi, a Chròch...
Peabar|4|True|Chan fhaigh mi ionnsachadh o na bana-ghoistidhean, cha dèan iad dad ach an taigh agam dhaib’ fhèin ’s gearan mu ’n cnàmhan cràidhteach ’s èigheachd orm ge b’ e dè nì mi ...
Cròch|6|True|Èist rium, ’s e bana-bhuidseach a th’ annad, giùlain thu fhèin mar tè!
Cròch|8|False|Mar nighean mhòr!
Cròch|7|True|Innis dhaibh sùil mun t-sròn!
Peabar|5|False|A bheil thu cinnteach nach urrainn dhut tighinn a-null agus mo chuideachadh ach am bruidhninn riutha ... ?
Cròch|9|False|Iochd! Thèid mi ann còmhla riut ma-thà a bhruidhinn riutha mu dhèidhinn.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tìom|1|False|Dè tha thu a’ ciallachadh nach eil thu ag ionnsachadh dad?!
Tìom|12|False|Agus chan eil thu fiù ’s dàna gu leòr fhèin gus seo dh’innse dhuinn?!
Tìom|14|False|Mura robh mo dhruim a’ gabhail rium, bheirinn an deagh-ionnsachadh dhut! Agus chan ann sa bhuidseachas, ’eil thu agam?!
Cìob|15|True|A Thìom, air do shocair
Cìob|16|False|Tha iad ceart ...
Eun|2|True|bìog
Eun|3|False|bìog
Eun|4|True|bìog
Eun|5|False|bìog
Eun|6|True|bìog
Eun|7|False|bìog
Eun|8|True|bìog
Eun|9|False|bìog
Eun|10|True|bìog
Eun|11|False|bìog
Fuaim|13|False|BEUM!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cìob|1|True|Chan eil sinn cho òg mar a bh’ àbhaist agus seo deagh àm oideachaidh.
Cìob|2|True|Mholainnsa gun coinnicheadh iad ri
Tìom|5|True|Hè hè!
Tìom|6|True|An Seanair!
Tìom|7|False|Nach eil iad ro òg ach an tuigeadh iad oideachadh-san?
Peabar|8|True|Ro òg airson dè!?
Peabar|9|False|Tèid sinn ann!
Cròch|10|False|“Sinne”?
Tìom|11|True|Seadh, seadh, seo an t-àite dhuibh!
Tìom|12|True|Leigidh mi leibh fhèin an aithne a chur air an t-Seanair.
Tìom|13|False|Lorgaidh sibh air bàrr an t-slèibh seo e.
Tìom|14|True|’Eil sibh deiseil airson
Tìom|15|False|ionnsachadh ceart?
Cìob|3|True|Seanair na Beinne
Cìob|4|False|Tha a chomhairle riatanach ...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|1|False|Deiseil!
Cròch|2|False|Is deònach!
Tìom|3|False|Taghta! Sin an dòigh!
Peabar|9|False|IONNSAIGH !!!
Cròch|6|False|Ui... UILEBHEIST!
Peabar|8|True|’S e RIBE a th’ ann !
Peabar|7|True|Nach robh mi ’n dùil air sin !!
Peabar|5|False|Gu sealladh sealbh orm !
Tìom|4|False|Dèanaibh às !

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|1|False|GRAVITAS SPIRALIS !
Cròch|2|False|BRASERO INTENSIA !
Fuaim|3|False|Splais!
Tìom|4|True|À, seo dhut òigridh an latha an-diugh ’s miann ionnsaighe orra an-còmhnaidh!
Peabar|7|False|Abair oideachadh craicte!
Urram|11|False|Ceadachas: Creative Commons Attribution 4.0, Bathar-bog: Krita, Blender, G'MIC, Inkscape air Ubuntu
Urram|10|False|Stèidhichte air saoghal Hereva a chaidh a chruthachadh le David Revoy le taic o Craig Maloney. Ceartachadh le Willem Sonke, Moini, Hali, CGand ’s Alex Gryson.
Neach-aithris|8|False|- Deireadh na sgeòil -
Urram|9|False|04/2016 – www.peppercarrot.com – Obair-ealain ⁊ sgeulachd: David Revoy – Eadar-theangachadh: GunChleoc
Tìom|6|False|Mur eil an latha a’ dol leibh, ’s fheàirrde sibh amar a ghabhail! Tha e maothach dh’ ar lòinidh ’s ur leithean.
Tìom|5|True|’S e oideachadh an t-Seanair gun dèan sibh na nì esan!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Urram|1|False|Tha Pepper&Carrot saor is le tùs fosgailte air fad agus ’ga sponsaireadh le taic nan leughadairean. Mòran taing dhan 671 pàtran a thug taic dhan eapasod seo:
Urram|2|False|’S urrainn dhut dol ’nad phàtran Pepper&Carrot cuideachd air www.patreon.com/davidrevoy
