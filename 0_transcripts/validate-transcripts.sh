#!/bin/bash
#
#  validate-transcripts.sh
#
#
#  SPDX-License-Identifier: GPL-3.0-or-later
#
#  Copyright 2019 GunChleoc <fios@foramnagaidhlig.net>
#
#  Script for validating Markdown transcript contents and their generation
#

scriptdir="0_transcripts"

# Move up if we're not in the base directory.
if [ -d "../$scriptdir" ]; then
    pushd ..
fi

# Run unit tests

0_transcripts/run_tests.py
if [[ ! $? -eq 0 ]] ; then
    echo " "
    echo "ERROR: Unit tests failed"
    exit 1
fi

# Run integration tests
declare -i errors; errors=0

# Find episodes/locales that have transcripts and validate them
for episodedir in *; do
    if [ -d "${episodedir}" ] ; then
        echo "Validating SVG and transcripts for $episodedir..."
        # We have a directory. Search for any locales
        for localedir in ${episodedir}/lang/*; do
            if [ -d "${localedir}" ] ; then
                # Only process directories that have transcripts in them
                mdfile_found=0
                locale="en"
                for mdfile in ${localedir}/ep*_transcript.md; do
                    # Get the locale from the file's path
                    locale=$(basename $(dirname $mdfile))

                    if [ -f "$mdfile" ] ; then
                        # There is a transcript for this episode and locale.
                        # Validate it and then break to the next locale.
                        mdfile_found=1

                        # Regenerate transcript to check if it will run through
                        # Output is noisy, so storing it in a variable to be printed on error only
                        logoutput=$($scriptdir/extract_text.py $episodedir $locale)
                        if [[ ! $? -eq 0 ]] ; then
                            echo " "
                            echo "ERROR: Updating transcript encountered an error for $episodedir $locale"
                            for line in "${logoutput[@]}" ; do
                              echo "$line"
                            done
                            errors+=1
                            echo " "
                        fi

                        # Generate HTML to check if it will run through
                        # Output is noisy, so storing it in a variable to be printed on error only
                        logoutput=$($scriptdir/extract_to_html.py $episodedir $locale)
                        if [[ ! $? -eq 0 ]] ; then
                            echo " "
                            echo "ERROR: Generating HTML encountered an error for $episodedir $locale"
                            for line in "${logoutput[@]}" ; do
                              echo "$line"
                            done
                            errors+=1
                            echo " "
                        fi
                        break
                    fi
                done
                # If we didn't regenerate the markdown, we still check if the svg parses for future transcripts
                if [ $mdfile_found -eq 0 ] ; then
                    $scriptdir/validate_xml.py $episodedir $locale
                    if [[ ! $? -eq 0 ]] ; then
                        echo "ERROR: Found invalid SVG for $episodedir $locale"
                        errors+=1
                    fi
                fi
            fi
        done
    fi
done


if [[ (${errors} > 0 )]] ; then
    echo "#####################"
    echo "  Found ${errors} error(s)"
    echo "#####################"
    exit 1
fi
